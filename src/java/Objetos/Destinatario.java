/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class Destinatario {
 
    private String rut;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private int telefono;
    private String email;
    private String rut_busqueda;
    private String rut_cli;
    
    public Destinatario(){
        
    }
    
    public Destinatario(String rut) {
        this.rut = rut;
    }
    
    public Destinatario(String rut, String rsocial, String direccion, String ciudad, int telefono, String email) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }
    
    public Destinatario(String rut, String rsocial, String direccion, String ciudad, int telefono, String email, String rut_cli ) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.rut_cli =rut_cli;
    }

    public Destinatario(String rut, String rut_busqueda, String rsocial, String direccion, String ciudad, int telefono, String email, String rut_cli) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.rut_busqueda = rut_busqueda;
        this.rut_cli =rut_cli;
    }

    public int insertDestinatarioCompleto() {
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "INSERT INTO destinatario VALUES(?,?,?,?,?,?,?)";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut);
            psipc.setString(2, this.rsocial);
            psipc.setString(3, this.direccion);
            psipc.setString(4, this.ciudad);
            psipc.setInt   (5, this.telefono);
            psipc.setString(6, this.email);
            psipc.setString(7, this.rut_cli);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Destinatario.insertDestinatarioCompleto(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//insertDestinatarioCompleto()
    
    public Destinatario buscarDestinatario() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "SELECT * FROM destinatario WHERE rut LIKE ?";
        Destinatario desRespuesta = null;

        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            psbpc.setString(1, this.rut);
            ResultSet rs = psbpc.executeQuery();

            if(rs.next()){
            String r = rs.getString(1);   // obtengo el rut desde la bd
            String rsc = rs.getString(2);
            String dir = rs.getString(3);
            String city = rs.getString(4);
            int fono = rs.getInt(5);
            String em = rs.getString(6);
            String rut_cli = rs.getString(7);
            desRespuesta = new Destinatario(r, rsc, dir, city, fono, em, rut_cli);
            }else{
                return null;
            }

        } catch (SQLException ex) {
            System.out.println("Cliente.buscarCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return desRespuesta;
        }
    }//buscarCliente()
    
    public void eliminarDestinatario() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlDelete = "DELETE destinatario WHERE rut LIKE ?";

        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlDelete);
            psbpc.setString(1, this.rut);
            psbpc.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Destinatario.eliminarDestinatario(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
        }
    }//eliminarDestinatario
    
    
    public int modificarDestinatario(){
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();

        String sqlUpdate = "UPDATE destinatario SET rut= ? "
                + ", rsocial= ? "
                + ", direccion= ? "
                + ", ciudad= ? "
                + ", telefono= ? "
                + ", email= ? "
                + ", rut_cliente= ? "
                + "WHERE rut = ?";

        try {
            PreparedStatement pscupd = oraconex.prepareStatement(sqlUpdate);
            pscupd.setString(1, this.rut);
            pscupd.setString(2, this.rsocial);
            pscupd.setString(3, this.direccion);
            pscupd.setString(4, this.ciudad);
            pscupd.setInt   (5, this.telefono);
            pscupd.setString(6, this.email);
            pscupd.setString(7, this.rut_cli);
            pscupd.setString(8, this.rut_busqueda);  //este valor es la id para modificar la fila, tambien considera si se cambia a si mismo
            filasAfectadas = pscupd.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Destinatario.modificarDestinatario(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//modificarDestinatario()
    
    public ArrayList llenarTablaDestinatario(){
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "SELECT * FROM destinatario";
        ArrayList<Destinatario> Destinatarios = new ArrayList<Destinatario>();
        int i =0;
        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();

            while(rs.next()){
                Destinatarios.add(new Destinatario(rs.getString(1), //rut
                        rs.getString(2),   //rsocial
                        rs.getString(3),   //direccion
                        rs.getString(4),   //ciudad
                        rs.getInt(5),      //telefono
                        rs.getString(6),   //email
                        rs.getString(7))); //rut_cli

                 
            }

        } catch (SQLException ex) {
            System.out.println("Destinatario.llenarTablaDestinatario(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return Destinatarios;
        }
    }//llenarTablaDestinatario()

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getRsocial() {
        return rsocial;
    }

    public void setRsocial(String rsocial) {
        this.rsocial = rsocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRut_busqueda() {
        return rut_busqueda;
    }

    public void setRut_busqueda(String rut_busqueda) {
        this.rut_busqueda = rut_busqueda;
    }

    public String getRut_cli() {
        return rut_cli;
    }

    public void setRut_cli(String rut_cli) {
        this.rut_cli = rut_cli;
    }
    
    
    
    @Override
    public String toString() {
        return    rut       + "|" 
                + rsocial   + "|" 
                + direccion + "|" 
                + ciudad    + "|" 
                + telefono  + "|" 
                + email     + "|"
                + rut_cli;
    }  
}
