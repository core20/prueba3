package Objetos;

import Utilidades.OracleConex;
import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ACliente {


    
    private String rut;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private int telefono;
    private String email;
    private int descuento;
    private String clasificacion;
    private String rut_busqueda;
    /*private String rut_prov;
    private String rut_trans;*/
    
    public ACliente(){
        
    }
    
    public ACliente(String rut){
       this.rut = rut; 
    }
    
    public ACliente(String rut, String rsocial, String direccion, String ciudad, int telefono, String email, int descuento, String clasificacion) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.descuento = descuento;
        this.clasificacion = clasificacion;
    }
    
    public ACliente(String rut, String rsocial, String direccion, String ciudad, int telefono, String email, int descuento, String clasificacion, String rut_busqueda) {
        //constructor creado con rut adicional en caso de modificarlo
        //no es la mejor solucion, pero funciona...
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.descuento = descuento;
        this.clasificacion = clasificacion;
        this.rut_busqueda = rut_busqueda;
    }
    
    /*public ACliente(String rut, String rsocial, String direccion, String ciudad, int telefono, String email, int descuento, String clasificacion, String rut_prov, String rut_trans) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.descuento = descuento;
        this.clasificacion = clasificacion;
        /*this.rut_prov = rut_prov;
        this.rut_trans = rut_trans;
    }*/
    
    /*public ACliente(String rut, String rsocial, String direccion, String ciudad, int telefono, String email, int descuento, String clasificacion, String rut_busqueda, String rut_prov, String rut_trans) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.descuento = descuento;
        this.clasificacion = clasificacion;
        this.rut_busqueda = rut_busqueda;
        /*this.rut_prov = rut_prov;
        this.rut_trans = rut_trans;
    }*/
    
    
    public int insertClienteCompleto() {
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "INSERT INTO cliente VALUES(?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut);
            psipc.setString(2, this.rsocial);
            psipc.setString(3, this.direccion);
            psipc.setString(4, this.ciudad);
            psipc.setInt   (5, this.telefono);
            psipc.setString(6, this.email);
            psipc.setInt   (7, this.descuento);
            psipc.setString(8, this.clasificacion);
            filasAfectadas = psipc.executeUpdate();
            
         /*if(this.rut_prov!=null && this.rut_trans!=null){
             try {
                String sqlInsertRelacionProv = "INSERT INTO PROVEEDOR VALUES(?,?)"; 
                PreparedStatement psirp = oraconex.prepareStatement(sqlInsertRelacionProv);
                psirp.setString(1, this.rut);
                psirp.setString(2, this.rut_prov);
                psirp.executeUpdate();

                String sqlInsertRelacionTrans = "INSERT INTO TRANSPORTISTA VALUES(?,?)"; 
                PreparedStatement psirt = oraconex.prepareStatement(sqlInsertRelacionTrans);
                psirt.setString(1, this.rut);
                psirt.setString(2, this.rut_trans);
                psirt.executeUpdate();
             } catch (SQLException ex) {
                 System.out.println("Cliente.insertClienteCompleto_relaciones(): " + ex.getMessage());
             }
         }
            */
        } catch (SQLException ex) {
            System.out.println("Cliente.insertClienteCompleto(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//insertClienteCompleto()
    
    public ACliente buscarCliente() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "SELECT * FROM cliente WHERE rut LIKE ?";
        ACliente cliRespuesta = null;

        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            psbpc.setString(1, this.rut);
            ResultSet rs = psbpc.executeQuery();

            if(rs.next()){
                String r = rs.getString(1);   // obtengo el rut desde la bd
                String rsc = rs.getString(2);
                String dir = rs.getString(3);
                String city = rs.getString(4);
                int fono = rs.getInt(5);
                String em = rs.getString(6);
                int dscto = rs.getInt(7);
                String cla = rs.getString(8);
            
            
                /*String Srut_prov="";
                try{
                    String sqlSelectRelacionProv = "select * from proveedor WHERE rut = ?"; 
                    PreparedStatement psirp = oraconex.prepareStatement(sqlSelectRelacionProv);
                    psirp.setString(1, r);
                    ResultSet rs2 = psirp.executeQuery();
                    rs2.next();
                    if(rs2!=null){
                        Srut_prov = rs2.getString("rut");  
                     }


                }catch(SQLException e){
                    System.out.println("Cliente.llenarTablaCliente_relacion(): " + e.getMessage());
                }

                String Srut_trans="";
                try{
                    String sqlSelectRelacionTrans = "select * from transportista  WHERE rut = ?"; 
                    PreparedStatement psirt = oraconex.prepareStatement(sqlSelectRelacionTrans);
                    psirt.setString(1, r);
                    ResultSet rs3 = psirt.executeQuery();
                    rs3.next();
                    if(rs3!=null){
                        Srut_trans = rs3.getString("rut");  
                    }
                }catch(SQLException e){
                    System.out.println("Cliente.llenarTablaCliente_relacion(): " + e.getMessage());
                }*/

                cliRespuesta = new ACliente(r, rsc, dir, city, fono, em, dscto, cla/*, Srut_prov, Srut_trans*/);
            }else{
                return null;
            }
            
        } catch (SQLException ex) {
            System.out.println("Cliente.buscarCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return cliRespuesta;
        }
    }//buscarCliente()
    
    public void eliminarCliente() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlDelete = "DELETE CLIENTE WHERE rut LIKE ?";
        
        /*
        try{
            String sqlDeleteRelacionProveedor = "DELETE CLIENTE_PROVEEDOR WHERE rut_cliente LIKE ?";
            String sqlDeleteRelacionTransportista = "DELETE CLIENTE_TRANSPORTISTA WHERE rut_cliente LIKE ?";
            String sqlDeleteRelacionDestinatario = "DELETE DESTINATARIO WHERE rut_cliente LIKE ?";
            PreparedStatement psdelprov = oraconex.prepareStatement(sqlDeleteRelacionProveedor);
            PreparedStatement psdeltrans = oraconex.prepareStatement(sqlDeleteRelacionTransportista);
            PreparedStatement psdeldes = oraconex.prepareStatement(sqlDeleteRelacionDestinatario);
            
            psdelprov.setString(1, this.rut);
            psdelprov.executeUpdate();
            
            psdeltrans.setString(1, this.rut);
            psdeltrans.executeUpdate();
            
            psdeldes.setString(1, this.rut);
            psdeldes.executeUpdate();
            
        }catch(SQLException e){
            System.out.println("Cliente.eliminarCliente_relaciones(): " + e.getMessage());
        }*/
        
        
        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlDelete);
            psbpc.setString(1, this.rut);
            psbpc.executeUpdate();
            psbpc.close();
        } catch (SQLException ex) {
            System.out.println("Cliente.eliminarCliente(): " + ex.getMessage());
        } finally {
            
            conora.closeConexionOracle();
        }
    }//eliminarCliente
    
    
    public int modificarCliente(){
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();

        String sqlUpdate = "UPDATE cliente SET rut= ? "
                + ", rsocial= ? "
                + ", direccion= ? "
                + ", ciudad= ? "
                + ", telefono= ? "
                + ", email= ? "
                + ", descuento= ? "
                + ", clasificacion= ? "
                + "WHERE rut = ?";

        try {
            PreparedStatement pscupd = oraconex.prepareStatement(sqlUpdate);
            pscupd.setString(1, this.rut);
            pscupd.setString(2, this.rsocial);
            pscupd.setString(3, this.direccion);
            pscupd.setString(4, this.ciudad);
            pscupd.setInt   (5, this.telefono);
            pscupd.setString(6, this.email);
            pscupd.setInt   (7, this.descuento);
            pscupd.setString(8, this.clasificacion);
            pscupd.setString(9, this.rut_busqueda);  //este valor es la id para modificar la fila, tambien considera si se cambia a si mismo
            filasAfectadas = pscupd.executeUpdate();
            
            /*try{
                if(this.rut_prov!=null && this.rut_trans!=null){
                    String sqlUpdateRelProv ="update PROVEEDOR set RUT_CLIENTE = ? , RUT_PROVEEDOR = ? WHERE RUT_CLIENTE = ?";
                    PreparedStatement psupdt_relProv = oraconex.prepareStatement(sqlUpdateRelProv);
                    psupdt_relProv.setString(1, this.rut);
                    psupdt_relProv.setString(2, this.rut_prov);
                    psupdt_relProv.setString(3, this.rut_busqueda);
                    psupdt_relProv.executeUpdate();

                    String sqlUpdateRelTrans ="update CLIENTE_TRANSPORTISTA set RUT_CLIENTE = ? , set RUT_TRANSPORTISTA = ? WHERE RUT_CLIENTE = ?";
                    PreparedStatement psupdt_relTrans = oraconex.prepareStatement(sqlUpdateRelTrans);
                    psupdt_relTrans.setString(1, this.rut);
                    psupdt_relTrans.setString(2, this.rut_trans);
                    psupdt_relTrans.setString(3, this.rut_busqueda);
                    psupdt_relTrans.executeUpdate();
                } 
              } catch (SQLException e) {
                 System.out.println("Ciente.modificarPersona_relaciones(): " + e.getMessage()); 
              } */ 
                
                
            
            
            
        } catch (SQLException ex) {
            System.out.println("Ciente.modificarPersona(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//modificarCliente()
    
    public String llenarTablaCliente(){
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "SELECT * FROM cliente";
        Gson gson = new Gson(); //Objeto que lleva a cabo las operaciones de transformación
        ArrayList<ACliente> Clientes = new ArrayList<ACliente>();
        String ClientesJSON = null;
        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();
            while(rs.next()){
                     
                    /*String Srut_prov="";
                    String Srut_trans="";
                    try{
                        String sqlSelectRelacionProv = "select * from cliente_proveedor  WHERE rut_cliente = ?"; 
                        PreparedStatement psirp = oraconex.prepareStatement(sqlSelectRelacionProv);

                        psirp.setString(1, rs.getString(1));
                        ResultSet rs2 = psirp.executeQuery();
                        rs2.next();
                        if(rs2!=null){
                          Srut_prov = rs2.getString(2);  
                        }
                        
                    
                        String sqlSelectRelacionTrans = "select * from cliente_transportista  WHERE rut_cliente = ?"; 
                        PreparedStatement psirt = oraconex.prepareStatement(sqlSelectRelacionTrans);

                        psirt.setString(1, rs.getString(1));
                        ResultSet rs3 = psirt.executeQuery();
                        rs3.next();
                        if(rs3!=null){
                          Srut_trans = rs3.getString(2);  
                        }
                    }catch(SQLException e){
                        System.out.println("Cliente.llenarTablaCliente_relacion(): " + e.getMessage());
                    }*/


                Clientes.add(new ACliente(rs.getString(1), //rut
                        rs.getString(2),   //rsocial
                        rs.getString(3),   //direccion
                        rs.getString(4),   //ciudad
                        rs.getInt(5),      //telefono
                        rs.getString(6),   //email
                        rs.getInt(7),      //descuento
                        rs.getString(8)/*, //clasificacion
                        Srut_prov,
                        Srut_trans*/));
                  ClientesJSON=gson.toJson(Clientes);
            }
        } catch (SQLException ex) {
            System.out.println("Cliente.llenarTablaCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return ClientesJSON;
        }
    }//llenarTablaCLientes()
        
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getRsocial() {
        return rsocial;
    }

    public void setRsocial(String rsocial) {
        this.rsocial = rsocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    /*public String getRut_prov() {
        return rut_prov;
    }

    public void setRut_prov(String rut_prov) {
        this.rut_prov = rut_prov;
    }

    public String getRut_trans() {
        return rut_trans;
    }

    public void setRut_trans(String rut_trans) {
        this.rut_trans = rut_trans;
    }*/
    
    //String rut, String rsocial, String direccion, String ciudad, int telefono, String email, int descuento, String clasificacion
    @Override
    public String toString() {
        return    rut       + "|" 
                + rsocial   + "|" 
                + direccion + "|" 
                + ciudad    + "|" 
                + telefono  + "|" 
                + email     + "|"
                + descuento + "|"
                + clasificacion + "|"
                + " " + "|" 
                + " " + "|";  }
    /*
        if(rut_prov!=null && rut_trans!=null){
          return    rut       + "|" 
                + rsocial   + "|" 
                + direccion + "|" 
                + ciudad    + "|" 
                + telefono  + "|" 
                + email     + "|"
                + descuento + "|"
                + clasificacion /*+ "|"
                + rut_prov  + "|"
                + rut_trans;  
        }else{
          return    rut       + "|" 
                + rsocial   + "|" 
                + direccion + "|" 
                + ciudad    + "|" 
                + telefono  + "|" 
                + email     + "|"
                + descuento + "|"
                + clasificacion + "|"
                + " " + "|" 
                + " " + "|";  
        }
        
        
    }*/
}
