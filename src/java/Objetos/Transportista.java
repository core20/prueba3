package Objetos;

import Utilidades.OracleConex;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Transportista {
         
    private String rut;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private int telefono;
    private String email;
    private String rut_busqueda;
        
    public Transportista(){
        
    }
    
    public Transportista(String rut){
        this.rut = rut;
    }

    public Transportista(String rut, String rsocial, String direccion, String ciudad, int telefono, String email) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }

    public Transportista(String rut, String rsocial, String direccion, String ciudad, int telefono, String email, String rut_busqueda) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
        this.rut_busqueda = rut_busqueda;
    }

    public int insertTransportistaCompleto() {
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "INSERT INTO transportista VALUES(?,?,?,?,?,?)";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut);
            psipc.setString(2, this.rsocial);
            psipc.setString(3, this.direccion);
            psipc.setString(4, this.ciudad);
            psipc.setInt   (5, this.telefono);
            psipc.setString(6, this.email);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Transportista.insertTransportistaCompleto(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//insertTransportistaCompleto()
    
    public Transportista buscarTransportista() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "SELECT * FROM transportista WHERE rut LIKE ?";
        Transportista transRespuesta = null;

        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            psbpc.setString(1, this.rut);
            ResultSet rs = psbpc.executeQuery();

            if(rs.next()){
                String r = rs.getString(1);   // obtengo el rut desde la bd
                String rsc = rs.getString(2);
                String dir = rs.getString(3);
                String city = rs.getString(4);
                int fono = rs.getInt(5);
                String em = rs.getString(6);
                transRespuesta = new Transportista(r, rsc, dir, city, fono, em);
            }else{
                return null;
            }
            
        } catch (SQLException ex) {
            System.out.println("Transportista.buscarTransportista(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return transRespuesta;
        }
    }//buscarTransportista()
    
    public void eliminarTransportista() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlDelete = "DELETE transportista WHERE rut LIKE ?";

        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlDelete);
            psbpc.setString(1, this.rut);
            psbpc.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Transportista.eliminarTransportistar(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
        }
    }//eliminarTransportista
    
    
    public int modificarTransportista(){
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();

        String sqlUpdate = "UPDATE transportista SET rut= ? "
                + ", rsocial= ? "
                + ", direccion= ? "
                + ", ciudad= ? "
                + ", telefono= ? "
                + ", email= ? "
                + "WHERE rut = ?";

        try {
            PreparedStatement pscupd = oraconex.prepareStatement(sqlUpdate);
            pscupd.setString(1, this.rut);
            pscupd.setString(2, this.rsocial);
            pscupd.setString(3, this.direccion);
            pscupd.setString(4, this.ciudad);
            pscupd.setInt   (5, this.telefono);
            pscupd.setString(6, this.email);
            pscupd.setString(7, this.rut_busqueda);  //este valor es la id para modificar la fila, tambien considera si se cambia a si mismo
            filasAfectadas = pscupd.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Transportista.modificarTransportista(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//modificarTransportista()
    
    public ArrayList llenarTablaTransportista(){
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "SELECT * FROM transportista";
        ArrayList Transportistas = new ArrayList();
        int i =0;
        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();

            while(rs.next()){
                Transportistas.add(new ArrayList());
                ((ArrayList)Transportistas.get(i)).add(rs.getString(1));  //rut
                ((ArrayList)Transportistas.get(i)).add(rs.getString(2));  //r social
                ((ArrayList)Transportistas.get(i)).add(rs.getString(3));  //direccion
                ((ArrayList)Transportistas.get(i)).add(rs.getString(4));  //ciudad
                ((ArrayList)Transportistas.get(i)).add(rs.getInt(5));     //telefono
                ((ArrayList)Transportistas.get(i)).add(rs.getString(6));  //email
                 i++;
            }

        } catch (SQLException ex) {
            System.out.println("Transportista.llenarTablaTransportista(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return Transportistas;
        }
    }//llenarTablaTransportista()
    
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getRsocial() {
        return rsocial;
    }

    public void setRsocial(String rsocial) {
        this.rsocial = rsocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRut_busqueda() {
        return rut_busqueda;
    }

    public void setRut_busqueda(String rut_busqueda) {
        this.rut_busqueda = rut_busqueda;
    }
    
    @Override
    public String toString() {
        return    rut       + "|" 
                + rsocial   + "|" 
                + direccion + "|" 
                + ciudad    + "|" 
                + telefono  + "|" 
                + email;
    }     
}
