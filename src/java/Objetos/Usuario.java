/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jorge
 */
public class Usuario {
    private String nombre_usuario;
    private int tipo;
    
    public Usuario(String nombre_usuario, String llave){
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlSelect = "SELECT * FROM usuario WHERE upper(nombre_usuario) LIKE upper(?) AND llave LIKE ?";
        PreparedStatement psbpc;
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            psbpc.setString(1, nombre_usuario);
            psbpc.setString(2, llave);
            ResultSet rs = psbpc.executeQuery();
            
            if(rs.next()){
                this.nombre_usuario = rs.getString("nombre_usuario");
                this.tipo = rs.getInt("tipo");
            }
        } catch (SQLException ex) {
            System.out.println("Usuario.processRequest(): "+ex.getMessage());
            connection.closeConexionOracle();
        }
        
        connection.closeConexionOracle();
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public int getTipo() {
        return tipo;
    }
    
    public boolean existe() {
        if(this.nombre_usuario != null){
            return true;
        }else{
            return false;
        }
    }
    
}
