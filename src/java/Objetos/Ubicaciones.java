/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

/**
 *
 * @author manuel
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import Utilidades.OracleConex;

public class Ubicaciones {
    
    private int id_Ubicacion;
    private String pasillo;
    private int ancho;
    private int largo;
    private int altura;
    private int estado;
    private int cod_reserva;
    private String rut;
    private String fecha_inicio;
    private String fecha_termino;
    private String nombre;
    
    public Ubicaciones() {
    }

    public Ubicaciones(int id_ubicacion, String pasillo, int ancho, int largo, int altura, int estado) {
        this.id_Ubicacion = id_ubicacion;
        this.pasillo = pasillo;
        this.ancho = ancho;
        this.largo = largo;
        this.altura = altura;
        this.estado = estado;

    }

    public Ubicaciones(int cod_reserva, String rut, String nombre, int id_ubicacion, String fecha_inicio, String fecha_termino) {
        this.cod_reserva = cod_reserva;
        this.rut = rut;
        this.nombre = nombre;
        this.id_Ubicacion = id_ubicacion;
        this.fecha_inicio = fecha_inicio;
        this.fecha_termino = fecha_termino;
    }

    public Ubicaciones[] disponibles() {

        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from UBICACION where estado=0";
        Ubicaciones ubicaciones = null;

        List<Ubicaciones> listaubicaciones = new ArrayList<Ubicaciones>();


        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();

            while (rs.next()) {
                this.id_Ubicacion = rs.getInt(1);   // obtengo id desde bd
                this.pasillo = rs.getString(2);
                this.ancho = rs.getInt(3);
                this.largo = rs.getInt(4);
                this.altura = rs.getInt(5);
                this.estado = rs.getInt(6);

                ubicaciones = new Ubicaciones(id_Ubicacion, pasillo, ancho, largo, altura, estado);
                listaubicaciones.add(ubicaciones);



            }
        } catch (SQLException ex) {
            System.out.println("cubicaciones.disponibles(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return listaubicaciones.toArray(new Ubicaciones[listaubicaciones.size()]);
        }
    }

    public Ubicaciones[] Nodisponibles() {

        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select r.COD_RESERVA, c.rut, p.rsocial, u.id_ubicacion, "
                + "r.fecha_inicio, r.fecha_termino from cliente c inner join "
                + "reserva r on c.rut=r.rut inner join ubicacion u on "
                + "u.id_ubicacion = r.id_ubicacion inner join"
                + " persona p on c.rut=p.rut where estado=1";
        Ubicaciones ubicaciones = null;

        List<Ubicaciones> listaubicaciones = new ArrayList<Ubicaciones>();


        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();

            while (rs.next()) {
                this.cod_reserva = rs.getInt(1);   // obtengo id desde bd
                this.rut = rs.getString(2);
                this.nombre = rs.getString(3);
                this.id_Ubicacion = rs.getInt(4);
                this.fecha_inicio = rs.getString(5);
                this.fecha_termino = rs.getString(6);

                ubicaciones = new Ubicaciones(cod_reserva, rut, nombre, id_Ubicacion, fecha_inicio, fecha_termino);
                listaubicaciones.add(ubicaciones);



            }
        } catch (SQLException ex) {
            System.out.println("Ubicacion.nodisponible(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return listaubicaciones.toArray(new Ubicaciones[listaubicaciones.size()]);
        }
    }

    public String reservar(int id_ubicacion, String rut, String fecha_i, String fecha_t) {

        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from cliente where rut like ?";
        
        
        
        System.out.println(rut + "....." + id_ubicacion);
        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            psbpc.setString(1, rut);
            System.out.println(psbpc.toString());
            ResultSet rs = psbpc.executeQuery();
            
            sqlSelect="select * from reserva where id_ubicacion like ?";
            psbpc = oraconex.prepareStatement(sqlSelect);
            psbpc.setInt(1, id_ubicacion);
            ResultSet rs1= psbpc.executeQuery();

            if (rs.next() && rs1.next()==false) {
                sqlSelect = "insert into reserva values(CODIGO_INC.nextval,?,?,?,?)";//

               
                PreparedStatement psipc = oraconex.prepareStatement(sqlSelect);
                psipc.setInt(1, id_ubicacion);
                psipc.setString(2, rut);
                psipc.setString(3, fecha_i);
                psipc.setString(4, fecha_t);

                psipc.execute();

                sqlSelect = "update UBICACION set estado=1 where id_ubicacion=" + id_ubicacion;
                psipc = oraconex.prepareStatement(sqlSelect);
                psipc.executeUpdate();

                System.out.println(psipc.toString());
                return "Se ha realizado la reserva";
            }
            
        } catch (SQLException ex) {
            return "No se ha realizado la reserva";
        } finally {
            conora.closeConexionOracle();
            
        }
        return "No se ha realizado la reserva";


    }

    public String Desocupar(int id_ubicacion) {

        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "update UBICACION set estado=0 where id_ubicacion=" + id_ubicacion;
        System.out.println(rut + "....." + id_ubicacion);
        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);

            psbpc.executeUpdate();
            
            sqlSelect="delete from reserva where id_ubicacion="+id_ubicacion;
            
            psbpc= oraconex.prepareStatement(sqlSelect);
            psbpc.executeUpdate();
            return "Se ha desocupado una Ubiacacion";


        } catch (SQLException ex) {
            System.out.println("Ubicaciones.desocupar(): " + ex.getMessage());
            return "No se pudo desocupar la Ubicacion";
        } finally {
            conora.closeConexionOracle();

        }
        


    }

 

   

    public int getId_Ubicacion() {
        return id_Ubicacion;
    }

    public void setId_Ubicacion(int id_Ubicacion) {
        this.id_Ubicacion = id_Ubicacion;
    }

    public String getPasillo() {
        return pasillo;
    }

    public void setPasillo(String pasillo) {
        this.pasillo = pasillo;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getLargo() {
        return largo;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getCod_reserva() {
        return cod_reserva;
    }

    public void setCod_reserva(int cod_reserva) {
        this.cod_reserva = cod_reserva;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_termino() {
        return fecha_termino;
    }

    public void setFecha_termino(String fecha_termino) {
        this.fecha_termino = fecha_termino;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
