/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author jorge
 */

public class EntradaMercaderia {
    String rut_cliente;
    ArrayList articulos;
    String codigo_entrada;
    
    private class ArticuloTemporal{
        String codigo_articulo;
        String cantidad;
        String ubicacion;
        String rut_pvd;

        public String getCodigo_articulo() {
            return codigo_articulo;
        }

        public String getCantidad() {
            return cantidad;
        }

        public String getUbicacion() {
            return ubicacion;
        }

        public String getRut_pvd() {
            return rut_pvd;
        }
        
    }

    public String getRut_cliente() {
        return rut_cliente;
    }

    public ArrayList getArticulos() {
        return articulos;
    }

    public String getCodigo_entrada() {
        return codigo_entrada;
    }
    
    public String getArrayValues(){
        String salida="";
        for(int i=0;    i<articulos.size();   i++) {
            salida += articulos.get(i);
        }
        return salida;
    }
    
    public int insertSQL(){
        int filasAfectadas = 0;
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlInsert = "INSERT INTO entrada_mercaderia VALUES (?,?,?,?)";
        try {
            PreparedStatement psipc = oracleconex.prepareStatement(sqlInsert);
            
            psipc.setString(1, this.codigo_entrada);
            psipc.setString(2, this.codigo_entrada);
            psipc.setString(3, this.rut_cliente);
            psipc.setString(4, "AB1010");
            
            filasAfectadas = psipc.executeUpdate();
            
            Gson gson = new Gson();
            for(int i=0;    i<this.articulos.size();   i++) {
                ArticuloTemporal arti = gson.fromJson(this.articulos.get(i).toString(), ArticuloTemporal.class);
                
                sqlInsert = "INSERT INTO detalle_entrada VALUES (?,?,?,?)";
                psipc = oracleconex.prepareStatement(sqlInsert);
                
                psipc.setString(1, arti.getCantidad());
                psipc.setString(2, this.codigo_entrada);
                psipc.setString(3, arti.getCodigo_articulo());
                psipc.setString(4, arti.getRut_pvd());
                
                filasAfectadas += psipc.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.println("Persona_Prueba.insertPersonaCompleta(): " + ex.getMessage());
        } finally {
            connection.closeConexionOracle();
            return filasAfectadas;
        }
        
    }
    
}
