/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jorge
 */
public class Persona {
    private String rut;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;

    public Persona() {
    }

    public Persona(String rut) {
        this.rut = rut;
    }

    public Persona(String rut, String rsocial, String direccion, String ciudad, String telefono, String email) {
        this.rut = rut;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }
    
    
    public Persona getFromSQL() {
        Persona pTemp = null;
        
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlSelect = "SELECT * FROM persona WHERE rut LIKE ?";
        PreparedStatement psbpc;
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            psbpc.setString(1, this.rut);
            ResultSet rs = psbpc.executeQuery();
            rs.next();
            
            this.rut = rs.getString("rut");
            this.rsocial = rs.getString("rsocial");
            this.direccion = rs.getString("direccion");
            this.ciudad = rs.getString("ciudad");
            this.telefono = rs.getString("telefono");
            this.email = rs.getString("email");
        } catch (SQLException ex) {
            System.out.println("Persona.getFromSQL(): "+ex.getMessage());
            connection.closeConexionOracle();
            return pTemp;
        }
        
        connection.closeConexionOracle();
        return pTemp;
    }

    public String getRut() {
        return rut;
    }

    public String getRsocial() {
        return rsocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }
    
    public String toJSON() {
        return (new GsonBuilder().setPrettyPrinting().create().toJson(this));
        /*return "{\"rut\":\""+this.rut+"\""
                +", \"rsocial\":\""+this.rsocial+"\""
                +", \"direccion\":\""+this.direccion+"\""
                +", \"ciudad\":\""+this.ciudad+"\""
                +", \"telefono\":\""+this.telefono+"\""
                +", \"email\":\""+this.email+"\""
                +"}";*/
    }
    
}
