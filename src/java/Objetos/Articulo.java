/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Nakkin
 */
public class Articulo {
    private String codigo_articulo;
    private String rut_pvd;
    private String descripcion;
    private String largo;
    private String ancho;
    private String alto;
    private String precio;
    private String estado_asegurado;
    private String id_clasificacion;
    private String id_ubicacion;
    private String rsocial;


    public Articulo() { 
    }
    
    public Articulo(String codigo_articulo) {
        this.codigo_articulo = codigo_articulo;
    }

    public Articulo(String codigo_articulo, String rut_pvd, String descripcion, String largo, String ancho, String alto, String precio, String estado_asegurado, String id_clasificacion, String id_ubicacion, String rsocial) {
        this.codigo_articulo = codigo_articulo;
        this.rut_pvd = rut_pvd;
        this.descripcion = descripcion;
        this.largo = largo;
        this.ancho = ancho;
        this.alto = alto;
        this.precio = precio;
        this.estado_asegurado = estado_asegurado;
        this.id_clasificacion = id_clasificacion;
        this.id_ubicacion = id_ubicacion;
        this.rsocial = rsocial;
    }
    
    public Articulo getFromSQL() {
        Articulo cTemp = null;
        
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();
  
        String sqlSelect = "SELECT * FROM articulo JOIN proveedor (articulo.rut_pvd = proveedor.rut_pvd) WHERE articulo.rut_pvd LIKE ?";
        PreparedStatement psbpc;    
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            psbpc.setString(1, this.rut_pvd);
            ResultSet rs = psbpc.executeQuery();
            rs.next();
         
            this.codigo_articulo = rs.getString("codigo_articulo");
            this.rut_pvd = rs.getString("rut.pvd");
            this.descripcion = rs.getString("descripcion");
            this.largo = rs.getString("largo");
            this.ancho = rs.getString("ancho");
            this.alto = rs.getString("alto");
            this.precio = rs.getString("precio");
            this.estado_asegurado = rs.getString("estado_asegurado");
            this.id_clasificacion = rs.getString("id_clasificacion");
            this.id_ubicacion = rs.getString("id_ubicacion");
            this.rsocial = rs.getString("rsocial");

        } catch (SQLException ex) {
            System.out.println("Articulo.getFromSQL(): "+ex.getMessage());
            connection.closeConexionOracle();
            return cTemp;
        }
        
        connection.closeConexionOracle();
        return cTemp;
    }
    
    public String getCodigo_articulo() {
        return codigo_articulo;
    }

    public String getRut_pvd() {
        return rut_pvd;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getLargo() {
        return largo;
    }

    public String getAncho() {
        return ancho;
    }

    public String getAlto() {
        return alto;
    }

    public String getPrecio() {
        return precio;
    }

    public String getEstado_asegurado() {
        return estado_asegurado;
    }

    public String getId_clasificacion() {
        return id_clasificacion;
    }

    public String getId_ubicacion() {
        return id_ubicacion;
    }

    public String getRsocial() {
        return rsocial;
    }
    
    public String toJSON() {
        return (new GsonBuilder().setPrettyPrinting().create().toJson(this));
    }
    
    
    }