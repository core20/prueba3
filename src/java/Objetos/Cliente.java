/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jorge
 */
public class Cliente {
    private String rut;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;
    private String descuento;
    private String clasificacion;

    public Cliente() {
    }

    public Cliente(String rut) {
        this.rut = rut;
    }
    
    
    public Cliente getFromSQL() {
        Cliente cTemp = null;
        
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlSelect = "SELECT * FROM cliente WHERE rut LIKE ?";
        PreparedStatement psbpc;
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            psbpc.setString(1, this.rut);
            ResultSet rs = psbpc.executeQuery();
            rs.next();
            
            this.rut = rs.getString("rut");
            this.rsocial = rs.getString("rsocial");
            this.direccion = rs.getString("direccion");
            this.ciudad = rs.getString("ciudad");
            this.telefono = rs.getString("telefono");
            this.email = rs.getString("email");
            this.descuento = rs.getString("descuento");
            this.clasificacion = rs.getString("clasificacion");
        } catch (SQLException ex) {
            System.out.println("Cliente.getFromSQL(): "+ex.getMessage());
            connection.closeConexionOracle();
            return cTemp;
        }
        
        connection.closeConexionOracle();
        return cTemp;
    }

    public String getRut() {
        return rut;
    }

    public String getRsocial() {
        return rsocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getDescuento() {
        return descuento;
    }

    public String getClasificacion() {
        return clasificacion;
    }
    
    public String toJSON() {
        return (new GsonBuilder().setPrettyPrinting().create().toJson(this));
        /*return "{\"rut\":\""+this.rut+"\""
                +", \"rsocial\":\""+this.rsocial+"\""
                +", \"direccion\":\""+this.direccion+"\""
                +", \"ciudad\":\""+this.ciudad+"\""
                +", \"telefono\":\""+this.telefono+"\""
                +", \"email\":\""+this.email+"\""
                +", \"descuento\":\""+this.descuento+"\""
                +", \"clasificacion\":\""+this.clasificacion+"\""
                +"}";*/
    }
    
}
