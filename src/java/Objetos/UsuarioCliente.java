/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import Utilidades.OracleConex;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jorge
 */
public class UsuarioCliente {
    private String rut;
    
    public UsuarioCliente(String rut, String llave){
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlSelect = "SELECT * FROM usuario_cliente JOIN cliente ON (usuario_cliente.rut = cliente.rut) WHERE usuario_cliente.rut LIKE ? AND usuario_cliente.llave LIKE ?";
        PreparedStatement psbpc;
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            psbpc.setString(1, rut);
            psbpc.setString(2, llave);
            ResultSet rs = psbpc.executeQuery();
            
            if(rs.next()){
                this.rut = rs.getString("rut");
            }
        } catch (SQLException ex) {
            System.out.println("UsuarioCliente.processRequest(): "+ex.getMessage());
            connection.closeConexionOracle();
        }
        
        connection.closeConexionOracle();
    }

    public String getRut() {
        return rut;
    }
    
    public boolean existe() {
        if(this.rut != null){
            return true;
        }else{
            return false;
        }
    }
    
}
