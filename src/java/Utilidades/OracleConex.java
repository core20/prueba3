/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author jorge
 */
public class OracleConex {
    private Connection oracon = null;
    private String ip;
    private String port;
    private String sid;
    private String url;
    private String user;
    private String pass;
    
    public OracleConex(){
        conectar();
    }
    
    // configura la conexion a Oracle y retorno el objeto conectado
    private Connection conectar(){
        ip="192.168.56.101"; // 127.0.0.1, localhost
        //ip="jorgearias.cl";
        port="1521";
        sid="XE";
        //driver@IPdeHost :port:SID ,userid ,password
        url="jdbc:oracle:thin:@"+ip+":"+port+":"+sid;
        user = "prueba3";
        pass="nasho";        
        System.out.println("URL: "+url);
        
        try{
            //Inicializar y registrar el Driver
            Class.forName("oracle.jdbc.driver.OracleDriver");
            
            //realizar la conexion
            oracon =DriverManager.getConnection(url,user,pass);
            System.out.println("conectado a Oracle: " +sid);
            return oracon;
        }catch(Exception ex){
            System.out.println("Error de conexion: OracleConex.conectar()"
                    + " no conectado: \n" 
                    +ex.getMessage());
            return null;
        }
    }//conectar()
    
    public Connection getConexionOracle(){
        return this.oracon;
    }    
    
    public void closeConexionOracle(){
        try {    
            this.oracon.close();
        } catch (Exception ex) {
             System.out.println("Error de desconexion: OracleConex.closeConexionOracle()");
        }        
    }
}
