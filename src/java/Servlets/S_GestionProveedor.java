/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Objetos.Proveedor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "S_GestionProveedor", urlPatterns = {"/S_GestionProveedor"})
public class S_GestionProveedor extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " +parametro_boton);
            
            if(parametro_boton.equalsIgnoreCase("btnBuscarProveedor")){ //buscar Proveedor
                String rut = request.getParameter("parametro_rut");
                String laBusqueda = buscarProveedor(rut);
                out.print(laBusqueda);
                return;  
                
            }else if(parametro_boton.equalsIgnoreCase("btnEliminarProveedor")){ //eliminar Proveedor
                String rut = request.getParameter("parametro_rut");
                Proveedor prov = new Proveedor(rut);
                prov.eliminarProveedor();
                return; 
                
            }else if(parametro_boton.equalsIgnoreCase("btnModificarProveedor")){   //modifica Proveedor
                String rut = request.getParameter("parametro_rut");
                String rsocial = request.getParameter("parametro_rsocial");
                String direccion = request.getParameter("parametro_direccion");
                String ciudad = request.getParameter("parametro_ciudad");
                String telefono = request.getParameter("parametro_telefono");
                int telefono_entero = Integer.parseInt(telefono);
                String email = request.getParameter("parametro_email");
                String rut_busqueda = request.getParameter("parametro_rut_busqueda"); //captamos el rut de busqueda 
                                                                                      //en caso de modificar la id      
                Proveedor prov = new Proveedor(rut, rsocial, direccion, ciudad, telefono_entero, email, rut_busqueda);
                //adicionalmente se envia el rut de la busqueda en caso de modificar el rut
                System.out.println("Proveedor a modificar : " + prov.toString());
                int filasModificadas = prov.modificarProveedor();
                System.out.println("filasModificadas: " + filasModificadas);
                String laBusqueda = buscarProveedor(rut);
                out.print(laBusqueda);
                return;
            
            
            }else if(parametro_boton.equalsIgnoreCase("btnGuardarProveedor")){ //ingresar Proveedor
                String rut = request.getParameter("parametro_rut");
                String rsocial = request.getParameter("parametro_rsocial");
                String direccion = request.getParameter("parametro_direccion");
                String ciudad = request.getParameter("parametro_ciudad");
                String telefono = request.getParameter("parametro_telefono");
                int telefono_entero = Integer.parseInt(telefono);
                String email = request.getParameter("parametro_email");
                
                Proveedor prov = new Proveedor(rut, rsocial, direccion, ciudad, telefono_entero, email);
                System.out.println("Cliente : " + prov.toString());
                int filasIngresadas = prov.insertProveedorCompleto();
                System.out.println("filasIngresadas: " + filasIngresadas);
                            
                if (filasIngresadas > 0) 
                    out.print("<p>Proveedor ingresado correctamente.</p><br />");
                else 
                    out.print("<span style='color:red'>Error, Proveedor no Ingresado.</span>");
            
            }else if(parametro_boton.equalsIgnoreCase("llenarTablaProveedor")){
                Proveedor prov = new Proveedor();
                ArrayList Proveedor = prov.llenarTablaProveedor();
                int i;
                for(i = 0; i < Proveedor.size();i++){ //recorre Arraylist
                    if(i %2==0 || i==0){ //es par
                        out.print("<tr class='light'>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(0)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(1)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(2)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(3)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(4)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(5)+"</td>"
                        + "</tr>");
                    }else if(i %2!=0 || i==1){  //es impar
                        out.print("<tr class='dark'>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(0)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(1)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(2)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(3)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(4)+"</td>"
                        + "<td>"+((ArrayList)Proveedor.get(i)).get(5)+"</td>"
                        + "</tr>");
                    }//if
                 }//for
                
            }  //else if(parametro_boton)
        } finally {            
            out.close();
        }
    }

    private String buscarProveedor(String rut){
        Proveedor prov = new Proveedor(rut);
        Proveedor provRespuesta = prov.buscarProveedor();
        
        if(provRespuesta != null){
            return provRespuesta.toString();
        }else
            return "error";        
    }    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
