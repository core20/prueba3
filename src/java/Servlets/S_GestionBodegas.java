package Servlets;

import Entidades.Bodega_Prueba;
import Entidades.Ubicacion_prueba;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jcf
 */
@WebServlet(name = "S_GestionBodegas", urlPatterns = {"/S_GestionBodegas"})
public class S_GestionBodegas extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);




            /**
             * *********************
             * Buscar Ubicaciones
       *********************
             */
            if (parametro_boton.equalsIgnoreCase("botonBuscarUbicacion")) {
                String id = "id_ubicacion =" + request.getParameter("parametro_id");
                System.out.println(id);
                String laBusqueda = BuscarUbicacion(id, "ubicacion");
                out.print(laBusqueda);
                return;
            }




            /**
             * *********************
             * Buscar Bodega (MUESTRA)
       *********************
             */
            if (parametro_boton.equalsIgnoreCase("botonBuscarBodega")) {

                String consulta = request.getParameter("parametro_consulta");
                String tabla = "datos_bodega";
                String laBusqueda = buscarBodega(consulta, tabla);
                //        System.out.println("respuestaa: "+laBusqueda);
                String datos[] = laBusqueda.split("-");

                out.print("<table border=5>");
                out.print("<tr>");
                out.print("<th>id_bodega</th><th>cantidad</th><th>superficie_total</th>");
                out.print("<tr>");
                for (int i = 0; i < datos.length; i++) {
                    out.print("<td>" + datos[i] + "</td>");
                    if ((i + 1) % 3 == 0) {
                        out.print("<tr>");
                    }
                }


                out.print("</table>");

                return;
            }

                /*
             * *********************
             * Insertar Bodega
      *********************
             */
            else if (parametro_boton.equalsIgnoreCase("botonGuardarBodega")) {
                String nombre_bodega = request.getParameter("parametro_nombre_bodega");
                Bodega_Prueba per = new Bodega_Prueba(nombre_bodega);
                int filasIngresadas = per.insertBodegaCompleta();
                if (filasIngresadas > 0) {
                    out.print(" <br>  <p style='color: yellow; font-size:18px'> Bodega ingresada "
                            + "correctamente <img src='img/ok2.gif'> </p>");
                } else {
                    out.print("<br><p style='color: red; font-size:18px'> Error, Bodega no ingresada  "
                            + "<img src='img/error.gif'> </p>");
                }

                return;
            } /**
             * *********************
             * Ver Bodegas (Muestra tabla)
       *********************
             */
            else if (parametro_boton.equalsIgnoreCase("verBodegas")) {
                String tabla = "bodega";
                String laBusqueda = BuscarTabla(tabla);
                String datos[] = laBusqueda.split("-");
                out.print("<table BORDER=10 CELLSPACING=5 CELLPADDING=10   >");
                out.print("<tr>");
                out.print("<th>id_bodega</th><th>Nombre_bodega</th>");
                out.print("<tr>");

                for (int i = 0; i < datos.length; i++) {
                    if (i % 2 == 0) {
                        out.print("<td> <A style='display: block; text-align:center;' HREF=# onclick=\"busqueda_ubicacion('id_bodega=" + datos[i] + "','presiona_celda');\"  >" + datos[i] + "</A></td>");
                    } else {
                        out.print("<td>" + datos[i] + "</td>");
                    }
                    if ((i + 1) % 2 == 0) {
                        out.print("<tr>");
                    }
                }
                out.print("</table>");

                return;
            } /**
             * *********************
             * Eliminar Bodegas
      *********************
             */
            else if (parametro_boton.equalsIgnoreCase("botonEliminarBodega")) {

                String id_bodega = request.getParameter("parametro_id_bodega");
                int id_bodega_entero = Integer.parseInt(id_bodega);
                
                Bodega_Prueba per = new Bodega_Prueba(id_bodega_entero);

                int filasEliminadas = per.deleteCompleta("bodega");

                if (filasEliminadas > 0) {
                    out.print(" <br><p style='color: yellow; font-size:18px'> Bodega eliminada correctamente"
                            + "  <img src='img/ok2.gif'> </p>");

                } else {
                    out.print("<br><p style='color: red; font-size:18px'> Error, Bodega no eliminada "
                            + " <img src='img/error.gif'> </p>");
                }

                return;
            } /**
             * *********************
             * Presionar Celda (Bodega, para ver sus ubicaciones)
       *********************
             */
            else if (parametro_boton.equalsIgnoreCase("presiona_celda")) {

                String consulta = request.getParameter("parametro_consulta");
                String tabla = "ubicacion";
                String laBusqueda = buscarBodega(consulta, tabla);
                String datos[] = laBusqueda.split("-");

                out.print("<table border=5>");
                out.print("<tr>");
                out.print("<th>id_ubicacion</th><th>Pasillo</th><th>Ancho</th><th>Largo</th><th>Altura</th><th>Estado</th><th>id_bodega</th><th>volumen</th><th>tamaño</th>");
                out.print("<tr>");

                for (int i = 0; i < datos.length; i++) {
                    out.print("<td>" + datos[i] + "</td>");
                    if ((i + 1) % 9 == 0) {
                        out.print("<tr>");
                    }
                }
                out.print("</table>");
                return;
            } 
            
            
            
            
       /***********************
          Insertar Ubicaciones
       **********************/
            else if (parametro_boton.equalsIgnoreCase("botonGuardarUbicacion")) {
                int id_bodega = Integer.parseInt(request.getParameter("parametro_id_bodega"));
                String pasillo = request.getParameter("parametro_pasillo");
                int ancho = Integer.parseInt(request.getParameter("parametro_ancho"));
                int largo = Integer.parseInt(request.getParameter("parametro_largo"));
                int altura = Integer.parseInt(request.getParameter("parametro_altura"));
                int estado = Integer.parseInt(request.getParameter("parametro_estado"));
                int volumen = ancho * largo * altura;


                String tamaño = "";
                if (volumen < 80) {
                    tamaño = "pequeño";
                } else if (volumen >= 80 && volumen <= 170) {
                    tamaño = "mediano";
                } else if (volumen > 170) {
                    tamaño = "grande";

                }
                    System.out.println("id bodega: " + id_bodega);
                Ubicacion_prueba per = new Ubicacion_prueba(id_bodega, pasillo, ancho, largo, altura, estado, volumen, tamaño);
                     //System.out.println("Ubicacion: " + per.toString());
                int filasIngresadas = per.insertUbicacionCompleta();
                     //System.out.println("filasIngresadas: " + filasIngresadas);
                if (filasIngresadas > 0) {
                    out.print(" <br>  <p style='color: yellow; font-size:18px'> Ubicación ingresada correctamente <img src='img/ok2.gif'> </p>");

                } else {
                    out.print("<br><p style='color: red; font-size:18px'> Id_Bodega no existente  <img src='img/error.gif'> </p>");
                }
                return;
            }
            
            

       /************************
          Actauliza Ubicaciones
        **********************/
          
            
            else if (parametro_boton.equalsIgnoreCase("botonActualizarUbicacion")) {
                int id_ubicacion = Integer.parseInt(request.getParameter("parametro_id_ubicacion"));
                int id_bodega = Integer.parseInt(request.getParameter("parametro_id_bodega"));
                String pasillo = request.getParameter("parametro_pasillo");
                int ancho = Integer.parseInt(request.getParameter("parametro_ancho"));
                int largo = Integer.parseInt(request.getParameter("parametro_largo"));
                int altura = Integer.parseInt(request.getParameter("parametro_altura"));
                int estado = Integer.parseInt(request.getParameter("parametro_estado"));
                int volumen = ancho * largo * altura;

                String tamaño = "";
                if (volumen > 80) {
                    tamaño = "pequeño";
                } else if (volumen <= 80 && volumen <= 170) {
                    tamaño = "mediano";
                } else if (volumen > 170) {
                    tamaño = "grande";

                }

               Ubicacion_prueba per = new Ubicacion_prueba(id_ubicacion, id_bodega, pasillo, ancho, largo, altura, estado, volumen, tamaño);
                int filasIngresadas = per.ActualizarUbicacion();
                System.out.println("filas ingresadas: "+filasIngresadas);
                if(filasIngresadas==0)
                    out.print("<br><p style='color: red; font-size:18px'> Ocurrio un error al actualizar </p>");
                else
                    out.print("<br><p style='color: green; font-size:18px'> Se actualizó correctamente</p>");
                return;
            } 
            
            
            
            
            
            
       /**********************
        Eliminar Ubicaciones
       *************************/
          
            else if (parametro_boton.equalsIgnoreCase("botonEliminarUbicacion")) {

                String id_ubicacion = request.getParameter("parametro_id_ubicacion");
                int id_ubicacion_entero = Integer.parseInt(id_ubicacion);
                Bodega_Prueba per = new Bodega_Prueba(id_ubicacion_entero);
                int filasEliminadas = per.deleteCompleta("ubicacion");
                System.out.println("filasEliminadas: " + filasEliminadas);
                if (filasEliminadas > 0) {
                    out.print(" <br><p style='color: yellow; font-size:18px'> Ubicación eliminada correctamente  <img src='img/ok2.gif'> </p>");

                } else {
                    out.print("<br><p style='color: red; font-size:18px'> Error, Ubicación no eliminada  <img src='img/error.gif'> </p>");
                }
                return;
            } else if (parametro_boton.equalsIgnoreCase("verUbicacion")) {
                String tabla = "ubicacion";
                String laBusqueda = BuscarTabla(tabla);
                out.print("<table border=5  >");
                out.print("<tr>");
                out.print("<th>id_ubicacion</th><th>Pasillo</th><th>Ancho</th><th>Largo</th><th>Altura</th><th>Estado</th><th>id_bodega</th>");
                out.print("<tr>");

                String datos[] = laBusqueda.split("-");

                for (int i = 0; i < datos.length; i++) {
                    out.print("<td>" + datos[i] + "</td>");
                    if ((i + 1) % 7 == 0) {
                        out.print("<tr>");
                    }
                }
                out.print("</table>");
                return;
            } 
            
            
            
            
            /**
             * *********************
             * Buscar Ubicaciones, dependiendo del filtrado
       *********************
             */
            else if (parametro_boton.equalsIgnoreCase("busca_ubicacion")) {
                String consulta = request.getParameter("parametro_consulta");
                String tabla = "ubicacion";
                String laBusqueda = buscarBodega(consulta, tabla);
                String datos[] = laBusqueda.split("-");

                out.print("<table border=5>");
                out.print("<tr>");
                out.print("<th>id_ubicacion</th><th>Pasillo</th><th>Ancho</th><th>Largo</th><th>Altura</th><th>Estado</th><th>id_bodega</th><th>volumen</th><th>tamaño</th>");
                out.print("</tr>");

                for (int i = 0; i < datos.length; i++) {
                    out.print("<td>" + datos[i] + "</td>");
                    if ((i + 1) % 9 == 0) {
                        out.print("<tr>");
                    }
                }
                out.print("</table>");

            }
            return;
        } catch (Exception e) {
            out.print("<p style='color:red'>Error: " + e.getMessage() + ".<br />Bodega no Ingresada.</p>");

        } finally {
            out.close();
        }


    }//processRequest()

    
    
    /*Metodo que hace la relacion con el metodo busca_bodega y permite verificar el valor de la respuesta :/ */
    private String buscarBodega(String consulta, String tabla) {
        Bodega_Prueba bod = new Bodega_Prueba();
        String Respuesta = bod.Buscar_Bodega(consulta, tabla);
        if (Respuesta != null) {
            return Respuesta;
        } else {
            return "error";
        }
    }
    
    private String BuscarUbicacion(String consulta, String tabla) {
        Bodega_Prueba bod = new Bodega_Prueba();
        String Respuesta = bod.Buscar_Ubicacion(consulta, tabla);
        if (Respuesta != null) {
            return Respuesta;
        } else {
            return "error";
        }
    }

  /*Metodo que hace la relacion con el metodo buscarTabla y permite verificar el valor de la respuesta :/ */
    private String BuscarTabla(String tabla) {
        Bodega_Prueba bod = new Bodega_Prueba();
        String Respuesta = bod.BuscarTabla(tabla);
        if (Respuesta != null) {
            return Respuesta;
        } else {
            return "error";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}