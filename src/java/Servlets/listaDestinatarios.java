/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Objetos.Destinatario;
import Utilidades.OracleConex;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jorge
 */
@WebServlet(name = "listaDestinatarios", urlPatterns = {"/listaDestinatarios"})
public class listaDestinatarios extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private class Destinatarios {
        ArrayList Destinatarios = new ArrayList();

        public Destinatarios() {
            Destinatarios = new ArrayList();
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlSelect = "SELECT * FROM destinatarios JOIN cliente  WHERE articulos.rut_pvd LIKE 0"+(request.getParameter("rut_pvd"));
        PreparedStatement psbpc;
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();
            
            //Gson gson = new Gson();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
            ArrayList Destinatarios = new ArrayList();
            
            /*while(rs.next()){
                Destinatarios.add(new Destinatario(
                        rs.getString("rut"),
                        rs.getString("rsocial"),
                        rs.getString("direccion"),
                        rs.getString("ciudad"),
                        rs.getString("telefono"),
                        rs.getString("email"),
                        rs.getString("rut_busqueda"),
                        rs.getString("rut_cli")
                        ));
            }*/
            
            
            out.print(gson.toJson(Destinatarios));
        } catch (SQLException ex) {
            System.out.println("listaDestinatarios.processRequest(): "+ex.getMessage());
            connection.closeConexionOracle();
        }
        
        connection.closeConexionOracle();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
