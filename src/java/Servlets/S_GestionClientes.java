package Servlets;

import Objetos.ACliente;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "S_GestionClientes", urlPatterns = {"/S_GestionClientes"})
public class S_GestionClientes extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String parametro_boton = request.getParameter("parametro_boton");
            
            if(parametro_boton.equalsIgnoreCase("btnBuscarCliente")){ //buscar cliente
                String rut = request.getParameter("parametro_rut");
                String laBusqueda = buscarCliente(rut);
                out.print(laBusqueda);
                return;  
                
            }else if(parametro_boton.equalsIgnoreCase("btnEliminarCliente")){ //eliminar cliente
                String rut = request.getParameter("parametro_rut");
                ACliente cli = new ACliente(rut);
                cli.eliminarCliente();
                return; 
                
            }else if(parametro_boton.equalsIgnoreCase("btnModificarCliente")){   //modifica cliente
                String rut = request.getParameter("parametro_rut");
                String rsocial = request.getParameter("parametro_rsocial");
                String direccion = request.getParameter("parametro_direccion");
                String ciudad = request.getParameter("parametro_ciudad");
                String telefono = request.getParameter("parametro_telefono");
                int telefono_entero = Integer.parseInt(telefono);
                String email = request.getParameter("parametro_email");
                String descuento = request.getParameter("parametro_dscto");
                int descuento_entero = Integer.parseInt(descuento);
                String clasificacion = request.getParameter("parametro_clasif");
                String rut_busqueda = request.getParameter("parametro_rut_busqueda"); //captamos el rut de busqueda 
                /*String rut_prov = request.getParameter("parametro_rut_prov");
                String rut_trans = request.getParameter("parametro_rut_trans");*/
                ACliente cli = null;
                
                cli = new ACliente(rut, rsocial, direccion, ciudad, telefono_entero, email, descuento_entero, clasificacion, rut_busqueda);
                /*
                if(rut_prov==null || rut_trans==null){                                                                      //en caso de modificar la id      
                    cli = new ACliente(rut, rsocial, direccion, ciudad, telefono_entero, email, descuento_entero, clasificacion, rut_busqueda);
                }else{
                   cli = new ACliente(rut, rsocial, direccion, ciudad, telefono_entero, email, descuento_entero, clasificacion, rut_busqueda, rut_prov, rut_trans); 
                }*/
                    
                //adicionalmente se envia el rut de la busqueda en caso de modificar el rut
                System.out.println("Cliente a modificar : " + cli.toString());
                int filasModificadas = cli.modificarCliente();
                System.out.println("filasModificadas: " + filasModificadas);
                String laBusqueda = buscarCliente(rut);
                out.print(laBusqueda);
                return;
            
            
            }else if(parametro_boton.equalsIgnoreCase("btnGuardarCliente")){ //ingresar cliente
                String rut = request.getParameter("parametro_rut");
                String rsocial = request.getParameter("parametro_rsocial");
                String direccion = request.getParameter("parametro_direccion");
                String ciudad = request.getParameter("parametro_ciudad");
                String telefono = request.getParameter("parametro_telefono");
                int telefono_entero = Integer.parseInt(telefono);
                String email = request.getParameter("parametro_email");
                String descuento = request.getParameter("parametro_dscto");
                int descuento_entero = Integer.parseInt(descuento);
                String clasificacion = request.getParameter("parametro_clasif");
                /*String rut_prov = request.getParameter("parametro_rut_prov");
                String rut_trans = request.getParameter("parametro_rut_trans");*/
                ACliente cli = null;
                
                cli = new ACliente(rut, rsocial, direccion, ciudad, telefono_entero, email, descuento_entero, clasificacion);
                
                /*if(rut_prov==null || rut_trans==null){
                    cli = new ACliente(rut, rsocial, direccion, ciudad, telefono_entero, email, descuento_entero, clasificacion);
                }else{
                    cli = new ACliente(rut, rsocial, direccion, ciudad, telefono_entero, email, descuento_entero, clasificacion, rut_prov, rut_trans);
                }*/
                
                System.out.println("Cliente : " + cli.toString());
                int filasIngresadas = cli.insertClienteCompleto();
                System.out.println("filasIngresadas: " + filasIngresadas);
                            
                if (filasIngresadas > 0) 
                    out.print("<p>Cliente ingresado correctamente.</p><br />");
                else 
                    out.print("<span style='color:red'>Error, Cliente no Ingresado.</span>");

            }else if(parametro_boton.equalsIgnoreCase("llenarTablaCliente")){
                try{
                    ACliente cli = new ACliente();
                String Cliente = cli.llenarTablaCliente();
                JsonParser parser = new JsonParser();
                JsonArray clienteJsonArray = parser.parse(Cliente).getAsJsonArray();
                JsonObject clienteJsonObject;
                int i;
                for (i=0;i< clienteJsonArray.size(); i++) {
                    clienteJsonObject = (JsonObject) clienteJsonArray.get(i);
                    if(i %2==0 || i==0){ //es par
                        out.print("<tr>");
                    }else if(i %2!=0 || i==1){  //es impar
                        out.print("<tr>"); 
                    }
                    out.print("<td>"+clienteJsonObject.get("rut").getAsString() +"</td>"
                    + "<td>"+clienteJsonObject.get("rsocial").getAsString()+"</td>"
                    + "<td>"+clienteJsonObject.get("direccion").getAsString()+"</td>"
                    + "<td>"+clienteJsonObject.get("ciudad").getAsString()+"</td>"        
                    + "<td>"+clienteJsonObject.get("telefono").getAsInt()+"</td>"
                    + "<td>"+clienteJsonObject.get("email").getAsString()+"</td>"
                    + "<td>"+clienteJsonObject.get("descuento").getAsInt()+"</td>"
                    + "<td>"+clienteJsonObject.get("clasificacion").getAsString()+"</td>"/*
                    + "<td>"+clienteJsonObject.get("rut_prov").getAsString()+"</td>" 
                    + "<td>"+clienteJsonObject.get("rut_trans").getAsString()+"</td>" 
                    */+ "</tr>");
                }//for
                }catch(Exception e){
                    out.println(e.getMessage() +"tabla");
                }
                
                
            }  //else if(parametro_boton)         
        } catch (Exception e) {
            out.println(e.getMessage() +"Cliente no Ingresado");
        } finally {            
            out.close();
        }
    }//process request
    
    private String buscarCliente(String rut){
        ACliente cli = new ACliente(rut);
        ACliente cliRespuesta = cli.buscarCliente();
        
        if(cliRespuesta != null){
            return cliRespuesta.toString();
        }else
            return "error";        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
