/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Objetos.Ubicaciones;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author manuel
 */
@WebServlet(name = "S_GestionUbicaciones", urlPatterns = {"/S_GestionUbicaciones"})
public class S_GestionUbicaciones extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           /* TODO output your page here. You may use following sample code. */

            Ubicaciones cu = new Ubicaciones();
            Ubicaciones[] lista = cu.disponibles();



            String datos = "<h3>Ubicaciones Disponibles</h3>"
                    + "<table class='Design4' border='1' width='700'>"
                    + "<tr>"
                    + "<th>Ubicacion</th>"
                    + "<th>Pasillo</th>"
                    + "<th>Ancho</th>"
                    + "<th>Largo</th>"
                    + "<th>Altura</th>"
                    + "<th>Estado</th>"
                    + "</tr>";
            for (int i = 0; i < lista.length; i++) {
                datos = datos + "<tr>"
                        + "<td>" + lista[i].getId_Ubicacion() + "</td>"
                        + "<td>" + lista[i].getPasillo() + "</td>"
                        + "<td>" + lista[i].getAncho() + "</td>"
                        + "<td>" + lista[i].getLargo() + "</td>"
                        + "<td>" + lista[i].getAltura() + "</td>"
                        + "<td><a href=\"reservarUbicacion.jsp?id_u=" + lista[i].getId_Ubicacion() + "\">Disponible</a></td>"
                        + "</tr>";
            }
            datos = datos + "</table>";
            out.print(datos);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
