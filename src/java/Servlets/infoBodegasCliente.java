/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Utilidades.OracleConex;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jorge
 */
@WebServlet(name = "infoBodegasCliente", urlPatterns = {"/infoBodegasCliente"})
public class infoBodegasCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        //String sqlSelect = "SELECT * FROM persona WHERE rut LIKE ?";
        String sqlSelect = "SELECT * FROM persona";
        PreparedStatement psbpc = oracleconex.prepareStatement(sqlSelect);
        //psbpc.setString(1, "174465614");
        ResultSet rs = psbpc.executeQuery();

        String rutObtenido = "";
        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("[");
            while(rs.next()){
                out.println("{\"rut\":\""+rs.getString("rut")+"\""
                        +", \"rsocial\":\""+rs.getString("rsocial")+"\""
                        +", \"rsocial\":\""+rs.getString(3)+"\""
                        +", \"rsocial\":\""+rs.getString(4)+"\""
                        +", \"rsocial\":\""+rs.getString(5)+"\""
                        +", \"rsocial\":\""+rs.getString(6)+"\""
                        +"}");
            }
            out.println("]");
        } finally {            
            out.close();
        }

        connection.closeConexionOracle();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(infoBodegasCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(infoBodegasCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
