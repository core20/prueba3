/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Objetos.Articulo;
import Utilidades.OracleConex;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jorge
 */
@WebServlet(name = "listaArticulos", urlPatterns = {"/listaArticulos"})
public class listaArticulos extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private class Articulos {
        ArrayList articulos = new ArrayList();

        public Articulos() {
            articulos = new ArrayList();
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");//EDITADO DE ORIGINAL PARA JSON
        response.setHeader("Content-Disposition", "inline");//AGREGADO PARA JSON
        PrintWriter out = response.getWriter();
        
        OracleConex connection = new OracleConex();
        Connection oracleconex = connection.getConexionOracle();

        String sqlSelect = "SELECT * FROM articulo JOIN proveedor ON (articulo.rut_pvd = proveedor.rut)"+((request.getParameter("contenga") != null)?" WHERE upper(articulo.descripcion) LIKE upper('%"+request.getParameter("contenga")+"%')":"");
        PreparedStatement psbpc;
        try {
            psbpc = oracleconex.prepareStatement(sqlSelect);
            ResultSet rs = psbpc.executeQuery();
            
            //Gson gson = new Gson();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();//Impresión con espacios y saltos de línea (para humanos)
            ArrayList articulos = new ArrayList();
            
            while(rs.next()){
                articulos.add(new Articulo(
                        rs.getString("codigo_articulo"),
                        rs.getString("rut_pvd"),
                        rs.getString("descripcion"),
                        rs.getString("largo"),
                        rs.getString("ancho"),
                        rs.getString("alto"),
                        rs.getString("precio"),
                        rs.getString("estado_asegurado"),
                        rs.getString("id_clasificacion"),
                        rs.getString("id_ubicacion"),
                        rs.getString("rsocial")
                        ));
            }
            out.print(gson.toJson(articulos));
        } catch (SQLException ex) {
            System.out.println("listaArticulos.processRequest(): "+ex.getMessage());
            connection.closeConexionOracle();
        }
        
        connection.closeConexionOracle();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
