package Servlets;

import Objetos.Destinatario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author drog
 */
@WebServlet(name = "S_GestionDestinatario", urlPatterns = {"/S_GestionDestinatario"})
public class S_GestionDestinatario extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " +parametro_boton);
            
            if(parametro_boton.equalsIgnoreCase("btnBuscarDestinatario")){ //buscar Destinatario
                String rut = request.getParameter("parametro_rut");
                String laBusqueda = buscarDestinatario(rut);
                out.print(laBusqueda);
                return;  
                
            }else if(parametro_boton.equalsIgnoreCase("btnEliminarDestinatario")){ //eliminar Destinatario
                String rut = request.getParameter("parametro_rut");
                Destinatario des = new Destinatario(rut);
                des.eliminarDestinatario();
                return; 
                
            }else if(parametro_boton.equalsIgnoreCase("btnModificarDestinatario")){   //modifica Destinatario
                String rut = request.getParameter("parametro_rut");
                String rsocial = request.getParameter("parametro_rsocial");
                String direccion = request.getParameter("parametro_direccion");
                String ciudad = request.getParameter("parametro_ciudad");
                String telefono = request.getParameter("parametro_telefono");
                int telefono_entero = Integer.parseInt(telefono);
                String email = request.getParameter("parametro_email");
                String rut_busqueda = request.getParameter("parametro_rut_busqueda"); //captamos el rut de busqueda 
                String rut_cli = request.getParameter("parametro_rut_cli");           //en caso de modificar la id      
                
                Destinatario des = new Destinatario(rut, rut_busqueda, rsocial, direccion, ciudad, telefono_entero, email, rut_cli);
                //adicionalmente se envia el rut de la busqueda en caso de modificar el rut
                System.out.println("Cliente a modificar : " + des.toString());
                int filasModificadas = des.modificarDestinatario();
                System.out.println("filasModificadas: " + filasModificadas);
                String laBusqueda = buscarDestinatario(rut);
                out.print(laBusqueda);
                return;
            
            
            }else if(parametro_boton.equalsIgnoreCase("btnGuardarDestinatario")){ //ingresar Destinatario
                String rut = request.getParameter("parametro_rut");
                String rsocial = request.getParameter("parametro_rsocial");
                String direccion = request.getParameter("parametro_direccion");
                String ciudad = request.getParameter("parametro_ciudad");
                String telefono = request.getParameter("parametro_telefono");
                int telefono_entero = Integer.parseInt(telefono);
                String email = request.getParameter("parametro_email");
                String rut_cli = request.getParameter("parametro_rut_cli");
 
                Destinatario des = new Destinatario(rut, rsocial, direccion, ciudad, telefono_entero, email, rut_cli);
                System.out.println("Cliente : " + des.toString());
                int filasIngresadas = des.insertDestinatarioCompleto();
                System.out.println("filasIngresadas: " + filasIngresadas);
                            
                if (filasIngresadas > 0) 
                    out.print("<p>Destinatario ingresado correctamente.</p><br />");
                else 
                    out.print("<span style='color:red'>Error al ingresar Destinatario, es posible que ya exista.</span>");
            }else if(parametro_boton.equalsIgnoreCase("llenarTablaDestinatario")){
                Destinatario des = new Destinatario();
                ArrayList<Destinatario> Destinatario = des.llenarTablaDestinatario();
                int i;
                for(i = 0; i < Destinatario.size();i++){ //recorre Arraylist
                    if(i %2==0 || i==0){ //es par
                        out.print("<tr>");
                    }else if(i %2!=0 || i==1){  //es impar
                        out.print("<tr>");
                    }
                        out.print( "<td>"+Destinatario.get(i).getRut() +"</td>"
                        + "<td>"+Destinatario.get(i).getRsocial()+"</td>"
                        + "<td>"+Destinatario.get(i).getDireccion()+"</td>"
                        + "<td>"+Destinatario.get(i).getCiudad()+"</td>"
                        + "<td>"+Destinatario.get(i).getTelefono()+"</td>"
                        + "<td>"+Destinatario.get(i).getEmail()+"</td>"
                        + "<td>"+Destinatario.get(i).getRut_cli()+"</td>"
                        + "</tr>");       
                 }//for
                
            }  //else if(parametro_boton)
        } finally {            
            out.close();
        }
    }

    private String buscarDestinatario(String rut){
        Destinatario des = new Destinatario(rut);
        Destinatario desRespuesta = des.buscarDestinatario();
        
        if(desRespuesta != null){
            return desRespuesta.toString();
        }else
            return "error";        
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
