package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import Utilidades.OracleConex;

public class Bodega_Prueba {

    private int id_bodega_pk;
    private String nombre_bodega;

    public Bodega_Prueba() {
    }

    public Bodega_Prueba(int id_bodega_pk) {
        this.id_bodega_pk = id_bodega_pk;
    }

    public Bodega_Prueba(String nombre_bodega) {
        this.nombre_bodega = nombre_bodega;

    }

    
    
//************************************************
//        METODO: INSERTA BODEGA
//*************************************************

    public int insertBodegaCompleta() {
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        /*id_bodega.NextVal es la secuncia creada en oracle para simular un autoincrement*/
        String sqlInsert = "insert into bodega values(id_bodega.NextVal, ?)";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.nombre_bodega);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Bodega_Prueba.insertBodegaCompleta(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    
//************************************************
//  no hay un metodo que elimina cualquiera de las dos?? este va?? 
//*************************************************
  
 

    
//************************************************
//  METODO: MUESTRA TABLA (segun parametros) 
//*************************************************
   
    public String Buscar_Bodega(String consulta, String tabla) {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from " + tabla + " where " + consulta;
        System.out.println(sqlSelect);
        String perRespuesta = "";

        try {
            Statement psbpc = oraconex.createStatement();
            ResultSet rs = psbpc.executeQuery(sqlSelect);
            //dependiendo del nombre de la tabla recibira de diferente forma la respuesta de la bd
            if (tabla == "datos_bodega") {
                while (rs.next()) {
                    perRespuesta = perRespuesta + "-" + rs.getString(1) + "-" + rs.getString(2)+"-"+rs.getString(3);
                }
            } else if (tabla == "ubicacion") {
                while (rs.next()) {
                    perRespuesta = perRespuesta + "-" + rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getInt(3)
                            + "-" + rs.getInt(4) + "-" + rs.getInt(5) + "-" + rs.getInt(6) + "-" + rs.getInt(7);
                }
            }

            perRespuesta = perRespuesta.substring(1);
           
        } catch (SQLException ex) {
            System.out.println("Buscarbodega(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return perRespuesta;
        }
    }
public String Buscar_Ubicacion(String consulta, String tabla) {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from " + tabla + " where " + consulta;
        String perRespuesta = "";

        try {
            Statement psbpc = oraconex.createStatement();
            ResultSet rs = psbpc.executeQuery(sqlSelect);
            //dependiendo del nombre de la tabla recibira de diferente forma la respuesta de la bd
            if (tabla == "datos_bodega") {
                while (rs.next()) {
                    perRespuesta = perRespuesta + "-" + rs.getString(1) + "-" + rs.getString(2)+"-"+rs.getString(3);
                }
            } else if (tabla == "ubicacion") {
                while (rs.next()) {
                    perRespuesta = perRespuesta + "-" + rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getInt(3)
                            + "-" + rs.getInt(4) + "-" + rs.getInt(5) + "-" + rs.getInt(6) + "-" + rs.getInt(7);
                }
            }

            perRespuesta = perRespuesta.substring(1);
           
        } catch (SQLException ex) {
            System.out.println("Buscarbodega(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return perRespuesta;
        }
    }
    
//************************************************
// Buscar tabla de ubicacion o bodegas 
//*************************************************

    public String BuscarTabla(String tabla) {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from " + tabla + " order by id_" + tabla;
        String perRespuesta = "";
        try {
            Statement psbpc = oraconex.createStatement();
            ResultSet rs = psbpc.executeQuery(sqlSelect);
            int i = 1;
            if (tabla == "bodega") {
                while (rs.next()) {
                    perRespuesta = perRespuesta + "-" + rs.getString(1) + "-" + rs.getString(2);
                }
            } else if (tabla == "ubicacion") {
                while (rs.next()) {
                      perRespuesta = perRespuesta + "-" + rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getInt(3)
                            + "-" + rs.getInt(4) + "-" + rs.getInt(5) + "-" + rs.getInt(6) + "-" + rs.getInt(7);
                }
            }
            perRespuesta = perRespuesta.substring(1);
        } catch (SQLException ex) {
            System.out.println("Buscarbodega(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return perRespuesta;
        }
    }

    
//************************************************
//   ELIMINA UBICACION O BODEGA (segun parametro)
//************************************************
    
    public int deleteCompleta(String tabla) {
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "delete from " + tabla + " where id_" + tabla + "=(?)";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.id_bodega_pk);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Bodega_Prueba.deleteBodegaCompleta(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    public int getId_bodega_pk() {
        return id_bodega_pk;
    }

    public void setId_bodega_pk(int id_bodega_pk) {
        this.id_bodega_pk = id_bodega_pk;
    }

    public String getNombre_bodega() {
        return nombre_bodega;
    }

    public void setNombre_bodega(String nombre_bodega) {
        this.nombre_bodega = nombre_bodega;
    }

    @Override
    public String toString() {
        return id_bodega_pk + "|" + nombre_bodega;
    }
}//class
