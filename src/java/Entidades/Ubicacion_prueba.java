/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Utilidades.OracleConex;

public class Ubicacion_prueba {

    private int id_ubicacion;
    private int id_bodega;
    private String pasillo;
    private int ancho;
    private int largo;
    private int altura;
    private int estado;
    private int volumen;
    private String tamaño;

    public Ubicacion_prueba() {
    }

    public Ubicacion_prueba(int id_bodega, String pasillo, int ancho, int largo, int altura, int estado, int volumen, String tamaño) {
        this.id_bodega = id_bodega;
        this.pasillo = pasillo;
        this.ancho = ancho;
        this.largo = largo;
        this.altura = altura;
        this.estado = estado;
        this.volumen = volumen;
        this.tamaño = tamaño;
    }

    public Ubicacion_prueba(int id_ubicacion, int id_bodega, String pasillo, int ancho, int largo, int altura, int estado, int volumen, String tamaño) {
        this.id_ubicacion = id_ubicacion;
        this.id_bodega = id_bodega;
        this.pasillo = pasillo;
        this.ancho = ancho;
        this.largo = largo;
        this.altura = altura;
        this.estado = estado;
        this.volumen = volumen;
        this.tamaño = tamaño;
    }

    public int getId_ubicacion() {
        return id_ubicacion;
    }

    public void setId_ubicacion(int id_ubicacion) {
        this.id_ubicacion = id_ubicacion;
    }

    public int getId_bodega() {
        return id_bodega;
    }

    public void setId_bodega(int id_bodega) {
        this.id_bodega = id_bodega;
    }

    public String getPasillo() {
        return pasillo;
    }

    public void setPasillo(String pasillo) {
        this.pasillo = pasillo;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getLargo() {
        return largo;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getVolumen() {
        return volumen;
    }

    public void setVolumen(int volumen) {
        this.volumen = volumen;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    
    
    
   /**************************
      INSERTA UBICACIONES
    *************************/

    public int insertUbicacionCompleta() {
        int filasAfectadas = 0;
        int filasIdEncontradas=0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
       
        String compruebaIdBodega= "select * from bodega where id_bodega=?";
        String sqlInsert = "insert into ubicacion values(id_ubicacion.NextVal,?,?,?,?,?,?)";

        try {
          
            PreparedStatement psidb = oraconex.prepareStatement(compruebaIdBodega);
            psidb.setInt(1, this.id_bodega);
            filasIdEncontradas = psidb.executeUpdate();
            //filasIdEncontradas=1;
            System.out.println("¿Existe esa bodega? :"+filasIdEncontradas);               
            
            if(filasIdEncontradas>0){ //Si existe la Bodega que inserte ubicacion            
            
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);   
            psipc.setString(1, this.pasillo);
            psipc.setInt(2, this.ancho);
            psipc.setInt(3, this.largo);
            psipc.setInt(4, this.altura);
            psipc.setInt(5, this.estado);
            psipc.setInt(6, this.id_bodega);
            //psipc.setInt(7, this.volumen);
            //psipc.setString(8, this.tamaño);
            filasAfectadas = psipc.executeUpdate();
            }
            else{System.out.println("No existe bodega");}
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Ubicacion_Prueba.insertUbicacionCompleta(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }//insertBodegaCompleta()

    
    
    
    
    
    
    
    
    
    /**************************
      ACTUALIZA UBICACIONES
    
    *************************/
  
    public int ActualizarUbicacion() {
        int filasAfectadas = 0;
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "update ubicacion set id_bodega= ?,pasillo =?,ancho=?,largo=?,altura=?,estado=? where id_ubicacion= ?";
        System.out.println(sqlInsert);
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.id_bodega);
            psipc.setString(2, this.pasillo);
            psipc.setInt(3, this.ancho);
            psipc.setInt(4, this.largo);
            psipc.setInt(5, this.altura);
            psipc.setInt(6, this.estado);          
            //psipc.setInt(7, this.volumen);
            //psipc.setString(8, this.tamaño);
            psipc.setInt(7, this.id_ubicacion);
            filasAfectadas = psipc.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Ubicacion_Prueba.insertUbicacionCompleta(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
    
    
    
       /**************************
      BUSCA UBICACIONES 
    
    *************************/
    
    public Ubicacion_prueba buscarUbicacion() {
        OracleConex conora = new OracleConex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from Ubicacion where id_ubicacion like ?";
        Ubicacion_prueba perRespuesta = null;

        try {
            PreparedStatement psbpc = oraconex.prepareStatement(sqlSelect);
            psbpc.setInt(1, this.id_ubicacion);
            ResultSet rs = psbpc.executeQuery();

            rs.next();
            int id_ubicacion = rs.getInt(1);   // obtengo el rut desde la bd
            int id_bodega = rs.getInt(2);
            String pasillo = rs.getString(3);
            int ancho = rs.getInt(4);
            int largo = rs.getInt(5);
            int altura = rs.getInt(6);
            int estado = rs.getInt(7);
            int volumen = rs.getInt(8);
            String tamaño = rs.getString(9);
          
            perRespuesta = new Ubicacion_prueba(id_ubicacion,id_bodega,pasillo,ancho,largo,altura,estado,volumen,tamaño);

        } catch (SQLException ex) {
            System.out.println("Ubicacion_prueba.buscarUbicacion(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return perRespuesta;
        }
    }
}




