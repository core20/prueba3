<%-- 
    Document   : reservarUbicacion
    Created on : 19-06-2013, 03:31:43 AM
    Author     : manuel
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("../index.jsp");
}
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Ubicaciones - Administración</title>
        <link href="../style/main.css" type="text/css" rel="stylesheet">
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">

        </script>
    </head>
    <body style="background: transparent;">
        <div id="registration" style="padding: 10px 30px;">
            <h1>Reserva de Ubicación</h1>

            <form id="reservaUbicacion" name="reservaUbicacion" action="" method="POST" enctype="multipart/form-data">

                <p>
                    <label>Ubicacion a reservar</label>
                    <input id="parametro_id" name="parametro_id" type="text" value="<%=request.getParameter("id_u")%>" OnFocus="this.blur()"/>
                </p>
                <p>
                    <label>Rut cliente</label>
                    <input id="parametro_rut" name="parametro_rut" type="text" />
                </p>
                <p>
                    <label>Fecha inicio</label>
                    <input id="parametro_fecha_i" name="parametro_fecha_i" type="text" />
                </p>
                <p>
                    <label>Fecha termino</label>
                    <input id="parametro_fecha_t" name="parametro_fecha_t" type="text" />
                </p>


                <p>
                    <input id="botonGuardar" type="button" class="button" onclick="reservarUbicacion();" value="Reservar">
                    <input id="botonReset" class="button" name="botonReset" type="reset" value="Limpiar Formulario" />             
                </p>
            </form>
        </div>
    </body>
</html>
