<%-- 
    Document   : listarUbicaciones
    Created on : 10-06-2013, 08:02:59 PM
    Author     : manuel
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("../index.jsp");
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Ubicaciones - Administraci�n</title>
        <link href="../style/main.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="../javascript/reservasJS/reservasJS.js"></script>
        <script type="text/javascript" src="../javascript/Validacion/validacionCliente.js"></script>  
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">

        </script>

    </head>   
    <body style="background: transparent;">

        <div id="registration" style="padding: 10px 30px;">

            <h1>Listar Ubicaciones</h1>
            <form id="filtroUbicaciones" name="formFiltroUbicaciones" action="" method="POST" enctype="multipart/form-data">
                <p>
                    <h2>Filtrar ubicaciones seg�n su estado</h2>
                    <input type="radio" name="opciones" id="rad1" value="libres"><label style="display: inline-block;">Ubicaciones libres</label> 
                    <input type="radio" name="opciones" id="rad2" value="ocupadas"><label style="display: inline-block;">Ubicaciones ocupadas</label> 
                    <input type="radio" name="opciones" id="rad3" value="porDesocupar"><label style="display: inline-block;">Ubicaciones por desocupar</label>
                </p>
                <br>
                <p>
                    <h2>Filtrar seg�n tama�o ubicaci�n en m�(volumen)</h2>
                    <input type="checkbox" name="checkbox"  id="chk1" value="tama�o='peque�o'"/>Peque�a(0-80m�)<br>
                    <input type="checkbox" name="checkbox"  id="chk2" value="tama�o='mediano'" />Mediana(81m�-170m�) <br>
                    <input type="checkbox" name="checkbox"  id="chk3" value="tama�o='grande'"/>Grande(mas de 170m�)<br>
                </p>
                <br>
                <p>
                    <h2>Filtrar seg�n tama�o ubicaci�n en m�(superficie)</h2>
                    <input type="checkbox" name="checkbox"  id="chk4" value="(ancho*largo) <= 40"/>Peque�a(0-40m�)<br>
                    <input type="checkbox" name="checkbox"  id="chk5" value="(ancho*largo) > 40 and (ancho*largo) <= 80" />Mediana(41m�-80m�) <br>
                    <input type="checkbox" name="checkbox"  id="chk6" value="(ancho*largo) > 80"/>Grande(mas de 80m�)<br>
                </p>
                <br>
                <p>
                <input type='button' id="parametro_boton" name="botonfiltraUbicacion"  class="classname" onclick="busqueda_filtrada('busca_ubicacion');
                limpia_checkbox('filtro');" value="Filtrar">
                <input type='button' id="parametro_boton" name="verUbicacion" class="classname" onclick=" Muestra_bodegas_o_ubicacions('verUbicaciones');
                limpia_checkbox('filtro2');" value='Ver todas'>
                </p>


            </form>


            <div id="muestraTablas">

                <div id="resultado">

                </div>
            </div><!-- Muestra Tablas-->

            <div id="div_respuesta">
            </div>
        </div>

    </body>
</html>
