<%-- 
    Document   : index
    Created on : 25-05-2013, 06:14:08 PM
    Author     : jorge
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style/main.css" type="text/css" rel="stylesheet">
        <title>Prueba 2 - Bodega</title>
    </head>
    <body>
        <h1>Índice de Usuarios:</h1>    
        
        
        <%
            OracleConex connection = new OracleConex();
            Connection oracleconex = connection.getConexionOracle();
            
            //String sqlSelect = "SELECT * FROM persona WHERE rut LIKE ?";
            String sqlSelect = "SELECT * FROM cliente";
            PreparedStatement psbpc = oracleconex.prepareStatement(sqlSelect);
            //psbpc.setString(1, "174465614");
            ResultSet rs = psbpc.executeQuery();

            /*String que guardará salida de cada fila recibida*/
            String rutObtenido = "";
            while(rs.next()){
                rutObtenido += "<a href=\"panelCliente.jsp?rut="+rs.getString("rut")+"\">"+rs.getString("rut")+"</a>";
                rutObtenido += " , "+rs.getString("rsocial");
                rutObtenido += " , "+rs.getString(3);
                rutObtenido += " , "+rs.getString(4);
                rutObtenido += " , "+rs.getString(5);
                rutObtenido += " , "+rs.getString(6);
                rutObtenido += "<br>";
            }
            
            connection.closeConexionOracle();
        %>
        <div id="indice-usuarios">
            <h3>Clientes:</h3>
            <p><%= rutObtenido%></p>
            <form id="form" name="form" action="" method="POST" enctype="multipart/form-data">
                
                <p><label for="username">Nombre de usuario</label>
                  <input type="text" value="" name="username" id="username">
                </p>
                <p>
                  <label for="password">Contraseña</label>
                  <input type="password" value="" name="password" id="password">
                </p>
                <p>
                    <label>Como:</label> 
                        <select name="como" id="como">
                            <option value="cliente" selected="">Cliente</option>
                            <option value="admin">Administrador</option>
                            <option value="bodega">Bodeguero</option>
                        </select>                    
                </p>
                  <input type="submit" value="Iniciar sesión" id="signin_submit">
            </form>
            
        
        </div>
    </body>
</html>