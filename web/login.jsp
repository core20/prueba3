<%@page import="Objetos.UsuarioCliente"%>
<%@page import="Objetos.Usuario"%>
<%
    //HttpSession session ya est� creado por defecto
    
    if(request.getParameter("logout") != null){
        session.invalidate();
        response.sendRedirect("index.jsp#logout");      //Invalida sesion
    }
    
    /*Si se envia formulario cliente se inicia session*/
    if(request.getParameter("cliente") != null){
        UsuarioCliente usuarioCliente = new UsuarioCliente(request.getParameter("rut-cliente"), request.getParameter("pass-cliente"));
        if(usuarioCliente.existe()){
            session.setAttribute("session-tipo", "clie");
            session.setAttribute("rut", usuarioCliente.getRut());
            
            response.sendRedirect("panelCliente.jsp");
        }else{
            response.sendRedirect("index.jsp#datos-erroneos-clie");
        }
    }else{
        /*Si se envia formulario administrador se inicia session*/
        if(request.getParameter("administrador") != null){
            Usuario usuario = new Usuario(request.getParameter("nombre-usuario"), request.getParameter("pass-administrador"));
            if(usuario.existe()){
                session.setAttribute("session-tipo", "admin");
                session.setAttribute("nombre-usuario", usuario.getNombre_usuario());
                session.setAttribute("usuario-tipo", usuario.getTipo());

                response.sendRedirect("seleccionar_operacion.jsp");
            }else{
                response.sendRedirect("index.jsp#datos-erroneos-admin");
            }
        }
    }
%>
