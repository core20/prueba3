var READY_STATE_COMPLETE = 4;
var peticion_http4;

function inicializa_objeto_ajax() {
    // retorno los objetos para trabajo con Ajax segun el navegador       
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            // to create XMLHttpRequest object in later versions
            // of Internet Explorer
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                // to create XMLHttpRequest object in older versions
                // of Internet Explorer
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }// catch
    }
}// fin()

function limpia_checkbox(id) {
    var checkboxes = document.getElementsByName("checkbox"); //Array que contiene los checkbox

    for (var x = 0; x < checkboxes.length; x++) {
        checkboxes[x].checked=false;
    }
}

//mostrar bodegas
function Muestra_bodegas_o_ubicacions(boton) {
    peticion_http4 = inicializa_objeto_ajax();
    var datos = "";
    datos = "parametro_boton=" + boton;
    console.log("datos: " + datos);
    console.log("antes onready");
    peticion_http4.onreadystatechange = procesaRespuestaBu;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");
    peticion_http4.open("POST", "/Prueba3/S_GestionBodegas", true);
    peticion_http4.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http4.setRequestHeader("Content-length", datos.length);
    peticion_http4.setRequestHeader("Connection", "close");
    peticion_http4.send(datos);
    return;
}




//Filtro de datos
function busqueda_filtrada(id, boton) {
    peticion_http = inicializa_objeto_ajax();
    var checkboxes = document.getElementsByName("checkbox"); //Array que contiene los checkbox
    var datos = "("; // concadenara la consulta que se envia a la bd
    var contador = 0; // cuenta la cantidad de checkbox activos

    //cantidad_ubica <= 10 or cantidad_ubica >10 and cantidad_ubica <=18 or cantidad_ubica>18
    for (var x = 0; x < checkboxes.length; x++) {
        if (checkboxes[x].checked) { // verifica si un checkbox esta o no activado
            contador++;
        }
    }
    var sql1=checkboxes[0].checked?"cantidad_ubica <= 10":"false";
    var sql2=checkboxes[1].checked?"cantidad_ubica >10 and cantidad_ubica <=18":"false";
    var sql3=checkboxes[2].checked?"cantidad_ubica>18":"false";
    if (contador !== 0) {
        datos = "parametro_boton=" + boton + "&parametro_consulta=("+sql1+" or "+sql2+" or "+sql3+")";
        /*
         * agrega un parentesis al final de la sentencia para evitar un error de sintaxis
         */


        console.log("datos: " + datos);
        console.log("antes onready");
        peticion_http.onreadystatechange = procesaRespuestaBu;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
        console.log("despues onready");
        peticion_http.open("POST", "/Prueba3/S_GestionBodegas", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);
    } else {
        alert("no seleciono campos para el filtrado");
    }
    return;
}// guardarPersona()


//  Recibe la repsuesta del servlet, y la muestra en una pagina o capa de la misma

function procesaRespuestaBu() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http4.readyState: " + peticion_http4.readyState);
    if (peticion_http4.readyState === READY_STATE_COMPLETE) {
        if (peticion_http4.status === 200) {
            var respuesta = peticion_http4.responseText;
            if (respuesta !== "error") {
                document.getElementById("resultado").innerHTML = respuesta;
            }
        }
    } else {
        document.getElementById("resultado").innerHTML = "Buscando...";
    }

    return;
}//fin()





