var READY_STATE_COMPLETE = 4;
var peticion_http;



function inicializa_objeto_ajax() {
    // retorno los objetos para trabajo con Ajax segun el navegador       
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            // to create XMLHttpRequest object in later versions
            // of Internet Explorer
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                // to create XMLHttpRequest object in older versions
                // of Internet Explorer
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }// catch
    }
}// fin()



// INSERTA BODEGA (captura los datos que permite ingresar una bodega)

function guardarBodega() {
    peticion_http = inicializa_objeto_ajax();
    var nombre_bodega = document.getElementById("parametro_nombre_bodega").value; //recibira solo el nombre de la bodega ya que la id es un autoincrement
    var datos = "parametro_boton=botonGuardarBodega&parametro_nombre_bodega=" + nombre_bodega;
    console.log("datos: " + datos);
    console.log("antes onready");
    peticion_http.onreadystatechange = procesaRespuestaGuad;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");
    peticion_http.open("POST", "/Prueba3/S_GestionBodegas", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');    
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);
    return;
}// guardarBodega()





// RESPUESTA. INSERTAR BODEGA

function procesaRespuestaGuad() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("capa").innerHTML = " <img src=  style='width: 36px; height: 36px;' > Enviando...";   
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta2 = peticion_http.responseText;
            document.getElementById("capa").innerHTML = respuesta2;
            setTimeout("document.location='ver_bodega.jsp';", 800);
            }
        } else {
            document.getElementById("capa").innerHTML = "proceso erroneo";
        }
    }
    return;
}//fin()










