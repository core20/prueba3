var READY_STATE_COMPLETE = 4;
var peticion_http2;

function inicializa_objeto_ajax() {
    // retorno los objetos para trabajo con Ajax segun el navegador       
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            // to create XMLHttpRequest object in later versions
            // of Internet Explorer
            xmlHttpReq2 = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                // to create XMLHttpRequest object in older versions
                // of Internet Explorer
                xmlHttpReq2 = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq2 = false;
            }
        }// catch
        return xmlHttpReq2;
    }
}// fin()


function busqueda_ubicacion(id_bodega, boton) {
    peticion_http2 = inicializa_objeto_ajax();
    var datos = "&parametro_boton=" + boton + "&parametro_consulta=" + id_bodega;
    console.log("datos: " + datos);
    console.log("antes onready");
    peticion_http2.onreadystatechange = procesaRespUbica;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");
    peticion_http2.open("POST", "/Prueba3/S_GestionBodegas", true);
    peticion_http2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http2.setRequestHeader("Content-length", datos.length);
    peticion_http2.setRequestHeader("Connection", "close");
    peticion_http2.send(datos);

    return;
}



function procesaRespUbica() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http2.readyState: " + peticion_http2.readyState);
    if (peticion_http2.readyState === READY_STATE_COMPLETE) {
        if (peticion_http2.status === 200) {
            var respuesta = peticion_http2.responseText;
            if (respuesta !== "error") {
                
                document.getElementById("resultado").innerHTML = respuesta;
            }
            }

        
    } else {
        document.getElementById("resultado").innerHTML = "Buscando...";
    }

    return;
}



//         ACTUALIZA UBICACION

function ActualizaUbicacion() {
    var id_ubicacion = document.getElementById("parametro_id").value;
    var id_bodega = document.getElementById("parametro_id_bodega").value;
    var pasillo = document.getElementById("parametro_pasillo").value;
    var ancho = document.getElementById("parametro_ancho").value;
    var largo = document.getElementById("parametro_largo").value;
    var altura = document.getElementById("parametro_altura").value;
    var estado = document.getElementById("parametro_estado").value;

// Valida los campos
    if (ancho.match(/^[a-zA-Z]+$/)){alert("Ancho inválido");}
    if (largo.match(/^[a-zA-Z]+$/)){alert("Largo inválido");}
    if (altura.match(/^[a-zA-Z]+$/)){alert("Altura inválido");}
    if (estado<0 || estado>1){alert("Estado inválido \n Desocupado=0\n Ocupado=1");} //Si no es 1 o 0
    else{
    
    peticion_http2 = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonActualizarUbicacion&parametro_id_ubicacion="+id_ubicacion
            +"&parametro_id_bodega="+id_bodega
            +"&parametro_pasillo="+pasillo
            +"&parametro_ancho="+ancho
            +"&parametro_largo="+largo
            +"&parametro_altura="+altura
            +"&parametro_estado="+estado;

    console.log("datos: " + datos);
    console.log("antes onready");
    peticion_http2.onreadystatechange = procesaUbicacion;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");
    peticion_http2.open("POST", "/Prueba3/S_GestionBodegas", true);
    peticion_http2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');    
    peticion_http2.setRequestHeader("Content-length", datos.length);
    peticion_http2.setRequestHeader("Connection", "close");
    peticion_http2.send(datos);}
return;
}




//  RESPUESTA ACTUALIZAR UBICACION

function procesaUbicacion() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http2.readyState: " + peticion_http2.readyState);
    if (peticion_http2.readyState == READY_STATE_COMPLETE)
        if (peticion_http2.status == 200) {
            var respuesta = peticion_http2.responseText;

                document.getElementById("div_respuesta").innerHTML = respuesta;
            }
        //alert(peticion_http2.readyState+","+peticion_http2.status+","+peticion_http2.responseText);
    /*
    if (peticion_http2.readyState === 1 || peticion_http2.readyState === 2 || peticion_http2.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http2.readyState);
       respuesta2 = peticion_http2.responseText;
       
    }
    else {
        if (peticion_http2.readyState === READY_STATE_COMPLETE) {
            if (peticion_http2.status === 200) {
                respuesta2 = peticion_http2.responseText;
            
            }
        } else {
                   
respuesta2 = peticion_http2.responseText;
       
        }
    }
    document.getElementById("div_respuesta").innerHTML=respuesta2;
    //alert(respuesta2);
    */
    
}//fin()

