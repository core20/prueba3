<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
      <head>
          <link rel="STYLESHEET" type="text/css" href="../style/main.css"> 
    <script type="text/javascript" src="jquery-1.7.1.min.js"></script>
  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">      
                <script type="text/javascript" src="js/eliminarBodegas.js"></script>   
         <script type="text/javascript" src="js/Guarda_Bodega.js"></script>
             <script type="text/javascript" src="js/Buscar_Bodega.js"></script>
      
             
        <script type="text/javascript">
            function init()
            {
            
            }
            function cambia(t)
            {
                var ids = ["#agregar_bodega","#eliminar_bodega"];
                $(ids[t?0:1]).hide();
                $(ids[t]).show();
            }
        </script>
        
        <style>
            #eliminar_bodega {display: none;}
        </style>
        
        
  <title>Mantencion Bodegas</title>  
    </head>

    
    
    <body onready='init()'>
        <div class="container" align='center'>
        <h1>Administrar Bodegas</h1>
        <div id="contenedor">
            
            <div>
                <label>Agregar Bodega<input id='agrega_check' onclick='cambia(0)' checked="checked" type="radio" name="selecciona_opcion" /></label>
                <label>Eliminar Bodega<input id='elimina_check' onclick='cambia(1)' type="radio" name="selecciona_opcion" value="Eliminar Bodega"/></label>
            </div>
            <div id='agregar_bodega'>
                <form class="register">   
                        Nombre Bodega :&nbsp;&nbsp;&nbsp;<input id="parametro_nombre_bodega" name="parametro_nombre_bodega" type="text" />
                        <div id="capa" > </div>

               
                <input type='button' id="botonGuardarBodega" class="classname" onclick="guardarBodega();" onmouseup= 'setTimeout("Muestra_bodegas_o_ubicacions(\'verBodegas\')",250);' value="Ingresar Bodega"/>
            </div>   
            <div id='eliminar_bodega'>
                    Id Bodega :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="parametro_id_bodega" name="parametro_id_bodega" type="text"/>
                    <div id="capa2" > </div>
                </form>
                <input type='button' id="botonEliminarBodega"  class="classname" onclick="eliminar('parametro_id_bodega','botonEliminarBodega');" onmouseup='setTimeout("Muestra_bodegas_o_ubicacions(\'verBodegas\')",250);' value="Eliminar Bodega"/>

            </div>

        </div>
        </div>
</body>
</html>
