

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
      <head>
          <link rel="STYLESHEET" type="text/css" href="../style/main.css"> 
    <script type="text/javascript" src="jquery-1.7.1.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">      
              <script type="text/javascript" src="js/Ingresar_Ubicacion.js"></script>
         <script type="text/javascript" src="js/eliminarBodegas.js"></script>
             <script type="text/javascript" src="js/Buscar_Bodega.js"></script>
              <script type="text/javascript" src="js/VerUbicacion.js"></script>
      
        <script type="text/javascript">
            function init()
            {
                Muestra_bodegas_o_ubicacions('verUbicacion');
            }
            function cambia(t)
            {
                var ids = ["#agregar_ubicacion","#eliminar_ubicacion"];
                $(ids[t?0:1]).hide();
                $(ids[t]).show();
            }
            function func()
            {
                setTimeout("Muestra_bodegas_o_ubicacions('verUbicacion');",500);
            }
        </script>
        
        <style>
            #eliminar_ubicacion {display: none;}
        </style>
        
        
  <title>Mantencion Bodegas</title>  
    </head>

    
    
    <body onready='init()'>
        <div class="container" align='center'>
            <h1>Administrar Ubicaciones</h1>
        <div id="contenedor">
            <div id="resultado" >
                </div>
            <div>
                <label>Agregar Ubicación<input id='agrega_check' onclick='cambia(0)' checked="checked" type="radio" name="selecciona_opcion" /></label>
                <label>Eliminar Ubicación<input id='elimina_check' onclick='cambia(1)' type="radio" name="selecciona_opcion" value="Eliminar Bodega"/></label>
            </div>
            <div id='agregar_ubicacion'>
                <table>
                    
                    <tr><td>Id Bodega</td><td align="left">:<input id="parametro_id_bodega" name="parametro_id_bodega" type="text" /></td></tr>
                    <tr><td>Pasillo</td><td align="left">:<input id="parametro_pasillo" name="parametro_pasillo" type="text" /></td></tr>
                    <tr><td>Ancho</td><td align="left">:<input id="parametro_ancho" name="parametro_ancho" type="text" /></td></tr>
                    <tr><td>Largo</td><td align="left">:<input id="parametro_largo" name="parametro_largo" type="text" /></td></tr>
                    <tr><td>Altura</td><td align="left">:<input id="parametro_altura" name="parametro_altura" type="text" /></td></tr>
                    <tr><td>Estado</td><td align="left">:<input id="parametro_estado" name="parametro_estado" type="text" /></td></tr>
                    
                </table>
                <input type='button' id="botonGuardarUbicacion" onclick="guardarUbicacion();" onmouseup= "func();" value="Ingresar Ubicación"/>
                <div id="capa" style="height: 64px; width: 300px;"> </div>
            </div>   
            <div id='eliminar_ubicacion'>
                <table>
                    <tr><td>Id Ubicacion </td><td>:<input id="parametro_id_ubicacion" name="parametro_id_ubicacion" type="text"/></td></tr>
            </table>
                    
                <input type='button' id="botonEliminarUbicacion"  onclick="eliminar('parametro_id_ubicacion','botonEliminarUbicacion');" onmouseup="func();" value="Eliminar Ubicación"/>
                <div id="capa2" style="height: 64px; width: 300px;"> </div>

            </div>

        </div>
        </div>
</body>
</html>
