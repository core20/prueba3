<%-- 
    Document   : panelAdmin
    Created on : 09-06-2013, 04:52:50 PM
    Author     : naxo
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("index.jsp");
}
%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page import="Objetos.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel Adminstrador</title>
        <link href="images/favicon.ico" rel="shortcut icon">
        <link href="style/main.css" type="text/css" rel="stylesheet">
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
       
    </head>
        <body>
            <div id="top-bar">
                <h1><a href="panelAdmin.jsp" style="text-decoration:none; color:inherit">Panel Administrador</a></h1><br />
                <div id="menu-options">
                    <div id="topnav">
                        <ul id="nav">
                            <li class="active"><a href="#">Cliente</a>
                                <ul>
                                    <li><a href="Agregar/Acliente.jsp" <%--data-hash="#Agregar/Acliente"--%>>Agregar</a></li>
                                    <li><a href="Modificar/Mcliente.jsp" <%--data-hash="#pagina2"--%>>Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Destinatario</a>
                                <ul>
                                    <li><a href="Agregar/Adestinatario.jsp">Agregar</a></li>
                                    <li><a href="Modificar/Mdestinatario.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Proveedor</a>
                                <ul>
                                    <li><a href="Agregar/Aproveedor.jsp">Agregar</a></li>
                                    <li><a href="Modificar/Mproveedor.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Transportista</a>
                                <ul>
                                    <li><a href="Agregar/Atransportista.jsp">Agregar</a></li>
                                    <li><a href="Modificar/Mtransportista.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="login.jsp?logout">Salir</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        
            <div id="content">
                <div id="info">
                        <h1>Bienvenido al modulo de Administración de Clientes</h1>
                        <p>
                            Para comenzar a administrar los clientes, acceda a algúno de los
                            enlaces de la barra de administración.
                        </p>                 
                </div>
            </div>
        </body>
</html>