<%-- 
    Document   : panelAdmin
    Created on : 09-06-2013, 04:52:50 PM
    Author     : naxo
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("index.jsp");
}
%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page import="Objetos.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel Adminstrador</title>
        <link href="images/favicon.ico" rel="shortcut icon">
        <link href="style/main.css" type="text/css" rel="stylesheet">
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            function modiframe(destino)
            {
                document.getElementById('mainframe').src=destino;
            }
        </script>
    </head>
        <body>
            <div id="top-bar">
                <h1><a href="panelAdmin.jsp" style="text-decoration:none; color:inherit">Panel Bodega</a></h1><br />
                <div id="menu-options">
                    <div id="topnav">
                        <ul id="nav">
                            <li class="active">
                                <a href="javascript:modiframe('AdminBodega/ver_bodega.jsp');">Ver Bodegas</a>
                            </li>
                            <li>
                                <a href="javascript:modiframe('AdminBodega/ingresar_eliminar_bodega.jsp');">Administrar Bodegas</a>
                            </li>
                            <li>
                                <a href="javascript:modiframe('AdminBodega/ingresar_eliminar_ubicaciones.jsp');">Administrar Ubicaciones</a>
                            </li>
                            <li>
                                <a href="javascript:modiframe('AdminBodega/actualizar.jsp');">Actualizar Datos</a>
                            </li>
                            <li><a href="login.jsp?logout">Salir</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        
            <div id="content">
                <div id="info">
                        <iframe id="mainframe" src="AdminBodega/index.jsp" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" style="display:block; height: 450px;">No tiene disponible el navegador la capacidad de iframe</iframe>
                </div>
            </div>
        </body>
</html>