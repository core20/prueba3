<%-- 
    Document   : index
    Created on : 25-05-2013, 06:14:08 PM
    Author     : jorge
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("index.jsp");
}
%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prueba 3 - Bodega</title>
        <link href="images/favicon.ico" rel="shortcut icon">
        <link href="style/index.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div id="header">
            <img src="images/Logo.png" alt="">
            <h1>Gestión Bodega v0.02</h1>
        </div>
        <div id="content">
            <div id="seleccionar-operacion">
                <h2>Seleccionar operación:</h2>
                <ul id="opciones">
                    <style type="text/css">li a {padding: 20px;}</style>
                    <li><a href="panelAdmin.jsp">Mantención de Clientes, Proveedores, Transportistas, Destinatarios</a></li>
                    <li><a href="panelBodega.jsp">Mantención de Bodegas/Ubicaciones</a></li>
                    <!--<li><a href="moduloUbicaciones/listarUbicaciones.jsp">Administración de Ubicaciones</a></li>-->
                </ul>
            </div>
        </div>
    </body>
</html>