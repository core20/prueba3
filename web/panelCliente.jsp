<%-- 
    Document   : panelCliente
    Created on : 25-05-2013, 10:26:44 PM
    Author     : jorge
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "clie"){
    response.sendRedirect("index.jsp");
}
%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page import="Objetos.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel Cliente - Bodegaje</title>
        <link href="images/favicon.ico" rel="shortcut icon">
        <link href="style/main.css" type="text/css" rel="stylesheet">
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="javascript/panelCliente.js"></script>
        <script type="text/javascript">
            $('#nueva-entrada').live('click', function(){           //Carga ventana flotante
                //$('#float-body').css('display','block');
                $('#float-body').addClass('activo');
                $('#space-load').html('<iframe src="darEntrada.jsp" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" style="display:block;">No tiene disponible el navegador la capacidad de iframe</iframe>');
                /*$('#space-load').load('darEntrada.jsp',function(response, status, xhr){
                    if (status == "error"){
                        var msg = "ERROR: ";
                        $('#space-load').prepend(msg+xhr.status+" "+xhr.statusText);
                    }
                });*/
                return false;
            });
            $('#nueva-salida').live('click', function(){
                //$('#float-body').css('display','block');
                $('#float-body').addClass('activo');
                $('#space-load').html('<iframe src="darSalida.jsp" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" style="display:block;">No tiene disponible el navegador la capacidad de iframe</iframe>');
                return false;
            });
        </script>
    </head>
    <body>
        <!--DIV Sobrepuesto-->
        <div id="float-body">
            <div id="content-float-body">
                <div id="float-body-closed"><a id="boton-cerrar" href=""><img src="images/close.png"></a></div>
                <div id="space-load"></div>
            </div>
        </div>
        <%if(session.getAttribute("rut") != null){
            Cliente cliente = new Cliente((String) session.getAttribute("rut"));
            cliente.getFromSQL();
        %>
            <div id="top-bar">
                <h1>Gestión Bodega v0.02 - Panel Cliente</h1><br />
                <div id="menu-options">
                    <ul id="nav">
                        <li><a id="nueva-entrada" href="#">Nueva Entrada</a></li
                        ><li><a id="nueva-salida" href="#">Dar Salida</a></li
                        ><li><a href="login.jsp?logout">Salir</a></li
                        >
                    </ul>
                </div>
            </div>
            <div id="content">
                <div id="info-cliente">
                    <h2>Mi Información</h2>
                        <table id="informacion-table">
                            <thead>
                                <tr><th></th><th></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Rut:</td>
                                    <td><%= cliente.getRut()%></td>
                                </tr>
                                <tr>
                                    <td>Razón Social:</td>
                                    <td><%= cliente.getRsocial()%></td>
                                </tr>
                                <tr>
                                    <td>Dirección:</td>
                                    <td><%= cliente.getDireccion()%></td>
                                </tr>
                                <tr>
                                    <td>Ciudad:</td>
                                    <td><%= cliente.getCiudad()%></td>
                                </tr>
                                <tr>
                                    <td>Teléfono:</td>
                                    <td><%= cliente.getTelefono()%></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td><%= cliente.getEmail()%></td>
                                </tr>
                                <tr>
                                    <td>Clasificación:</td>
                                    <td><%= cliente.getClasificacion()%></td>
                                </tr>
                                <tr>
                                    <td>Descuento:</td>
                                    <td><%= cliente.getDescuento()%></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
                <div id="ubicaciones-cliente">
                    <h2>Mis Ubicaciones</h2>
                    <%
                    OracleConex connection = new OracleConex();
                    Connection oracleconex = connection.getConexionOracle();

                    String sqlSelect = "SELECT * FROM entrada_mercaderia JOIN detalle_entrada ON (entrada_mercaderia.codigo_entrada = detalle_entrada.codigo_entrada) JOIN articulo ON (detalle_entrada.codigo_articulo = articulo.codigo_articulo) WHERE entrada_mercaderia.rut_cliente LIKE '"+session.getAttribute("rut")+"' AND detalle_entrada.cantidad > 0";
                    PreparedStatement psbpc;
                    
                    psbpc = oracleconex.prepareStatement(sqlSelect);
                    ResultSet rs = psbpc.executeQuery();
                    
                    out.println("<ul>");
                    while(rs.next()){
                        out.println("<li>"+rs.getString("descripcion")+" - $"+rs.getString("precio")+"<!-- / Ubicación: "+rs.getString("id_ubicacion")+"--></li>");
                    }
                    out.println("</ul>");
                    %>
                </div>
            </div>
        <%}//Fin IF
        else{%>
        <div id="error">
            <p>No ha entrado</p>
        </div>
        <%}//Fin ELSE%>
    </body>
</html>