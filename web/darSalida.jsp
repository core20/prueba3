<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "clie"){
    response.sendRedirect("index.jsp");
}

if(request.getParameter("codigoentrada") != null && request.getParameter("salidacantidad") != null){
    OracleConex connection = new OracleConex();
    Connection oracleconex = connection.getConexionOracle();
    
    PreparedStatement psipc = oracleconex.prepareStatement("UPDATE detalle_entrada SET detalle_entrada.cantidad = cantidad-"+request.getParameter("salidacantidad")+" WHERE detalle_entrada.codigo_entrada LIKE '"+request.getParameter("codigoentrada")+"'");
    int filasAfectadas = psipc.executeUpdate();
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dar entrada - Bodegaje</title>
        <link href="style/main.css" type="text/css" rel="stylesheet">
        <style type="text/css">
            #resultados li { border-bottom: 1px solid #AAA;}
            #resultados li a { text-decoration: none; color: blue;}
            #resultados li a:hover { text-decoration: none; color: purple; outline: none;}
        </style>
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            window.onload = function(){
                $("#div-detalle-entrada").addClass('active');
            };
            
            $('.soloNumero').live('keypress keyup blur', function(){
                /*Expresi�n regular que solo reemplaza d�gitos (letras)*/
                this.value = this.value.replace(/[^\d]*/g,'');
            });
            
            function agregarArticulo(codigo_articulo,rut_pvd,descripcion,rsocial){
                $('#detalle-entrada tbody').append('<tr>\
                                                        <input type="hidden" name="articulo[]" value="'+codigo_articulo+'">\
                                                        <input type="hidden" name="rut_pvd[]" value="'+rut_pvd+'">\
                                                        <td><input type="text" name="detalle" value="'+descripcion+' - '+rsocial+'" disabled="disabled"></td>\
                                                        <td><input type="text" class="cantidad soloNumero" name="articulo-cantidad[]" value=""></td>\
                                                        <td>\
                                                            <select class="id_ubicacion" name="id_ubicacion[]" onblur="comprobarUbicacionUnica(this);" onclick="comprobarUbicacionUnica(this);" disabled="disabled">\
                                                                <option></option>\
                                                            </select>\
                                                        </td>\
                                                        <td><a href="#" onclick="eliminarArticulo(this);"><img src="images/delete.png" style="height: 24px;"></a></td>\
                                                    </tr>');
                $("#div-detalle-entrada").addClass('active');
                $("#busqueda-articulos").css('display','none');
                agregaUbicacionesLibresBodega(ubicacionesOptions);
                return false;
            }
            
            function sacarArticulo(element){
                window.location.href = 'darSalida.jsp?codigoentrada='+$(element).parents('tr').find('.codigo-entrada').val()+'&salidacantidad='+$(element).parents('tr').find('.salida-cantidad').val();
                return false;
            }
            
            function comprobarUbicacionUnica(esteElemento){
                $('.id_ubicacion').each(function( index, element ) {
                    if(esteElemento !== element){
                        if(esteElemento.value === element.value){
                            element.value = '';
                        }
                    }
                });
            }
            
            $('#formEntrada #submit').live('click',function(){
                articulos = [];
                /*Para cada elemento selecciona sus atributos para agregar a array*/
                $('#detalle-entrada [name=articulo[]]').each(function( index, element ){
                    articulo = new Object();
                    articulo.codigo_articulo = element.value;
                    articulo.cantidad = $('#detalle-entrada [name=articulo-cantidad[]]')[index].value;
                    articulo.ubicacion = $('#detalle-entrada [name=id_ubicacion[]]')[index].value;
                    articulo.rut_pvd = $('#detalle-entrada [name=rut_pvd[]]')[index].value;
                    
                    if(articulo.cantidad>0 && articulo.ubicacion!==''){
                        articulos.push(articulo);
                    }else{
                        alert('�No has completado todos los datos!\nNo dejes art�culos vac�os.');
                        return false;
                    }
                });
                //articulos.push({rut:'<%= (String) session.getAttribute("rut")%>'});
                /*console.log(JSON.stringify({
                    articulos:articulos,
                    rut_cliente:'<%= (String) session.getAttribute("rut")%>',
                    codigo_entrada:Math.round((new Date()).getTime() / 1000).toString()}));*/
                $.getJSON( "insertarEntrada?json="+JSON.parse(JSON.stringify(JSON.stringify({
                    articulos:articulos,
                    rut_cliente:'<%= (String) session.getAttribute("rut")%>',
                    codigo_entrada:Math.round((new Date()).getTime() / 1000).toString()}))),
                    function( respuesta ) {
                        if(respuesta['cambios'] > 0){
                            alert("Entrada enviada correctamente!");
                            location.reload();
                        }
                });
                return false;
            });
        </script>
    </head>
    <body style="background: transparent;">
        <div id="registration" style="padding: 10px 30px;">
            <h1>Dar Salida</h1>
            <form id="formEntrada" name="formEntrada" action="" method="POST" enctype="multipart/form-data">
                <div id="div-detalle-entrada">
                    <label>Detalle</label>
                    <table id="detalle-entrada">
                        <thead>
                            <tr>
                                <th><label>Art�culos</label></th>
                                <th><label>Valor</label></th>
                                <th><label>Cantidad</label></th>
                                <th><label>Sacar</label></th>
                                <th><label>Dar Salida</label></th>
                            </tr>
                            <%
                            OracleConex connection = new OracleConex();
                            Connection oracleconex = connection.getConexionOracle();

                            String sqlSelect = "SELECT * FROM entrada_mercaderia JOIN detalle_entrada ON (entrada_mercaderia.codigo_entrada = detalle_entrada.codigo_entrada) JOIN articulo ON (detalle_entrada.codigo_articulo = articulo.codigo_articulo) WHERE entrada_mercaderia.rut_cliente LIKE '"+session.getAttribute("rut")+"' AND detalle_entrada.cantidad > 0";
                            PreparedStatement psbpc;

                            psbpc = oracleconex.prepareStatement(sqlSelect);
                            ResultSet rs = psbpc.executeQuery();

                            while(rs.next()){
                                out.println("<tr><input type=\"hidden\" class=\"codigo-entrada\" name=\"codigo-entrada\" value=\""+rs.getString("codigo_entrada")+"\" disabled=\"disabled\">"
                                        + "<td><input type=\"text\" name=\"detalle\" value=\""+rs.getString("descripcion")+"\" disabled=\"disabled\"></td>"
                                        + "<td><input type=\"text\" value=\"$"+rs.getString("precio")+"\" disabled=\"disabled\"></td>"
                                        + "<td><input type=\"text\" class=\"cantidad soloNumero\" name=\"articulo-cantidad[]\" value=\""+rs.getString("cantidad")+"\" disabled=\"disabled\"></td>"
                                        + "<td><input type=\"text\" class=\"salida-cantidad cantidad soloNumero\" name=\"salida-cantidad[]\" value=\"\"></td>"
                                        + "<td><a href=\"#\" onclick=\"sacarArticulo(this);\"><img src=\"images/dar-salida.png\" style=\"height: 48px;\"></a></td></tr>");
                            }
                            %>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </body>
</html>