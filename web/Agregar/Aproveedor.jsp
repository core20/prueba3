<%-- 
    Document   : Aproveedor
    Created on : 09-06-2013, 06:25:01 PM
    Author     : naxo
--%>

<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "admin"){
    response.sendRedirect("../index.jsp");
}
%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page import="Objetos.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel Adminstrador - Agregar Proveedor</title>
        <link href="../images/favicon.ico" rel="shortcut icon">
        <link href="../style/main.css" type="text/css" rel="stylesheet">
        
        <script type="text/javascript" src="../javascript/AdministracionJS/agregarProveedor.js"></script>
        <script type="text/javascript" src="../javascript/Validacion/validacion.js"></script>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
    </head>
        <body>       
            <div id="top-bar">
                <h1><a href="../panelAdmin.jsp" style="text-decoration:none; color:inherit">Panel Administrador</a></h1><br />
                <div id="menu-options">
                    <div id="topnav">
                        <ul id="nav">
                            <li><a href="#">Cliente</a>
                                <ul>
                                    <li><a href="../Agregar/Acliente.jsp">Agregar</a></li>
                                    <li><a href="../Modificar/Mcliente.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Destinatario</a>
                                <ul>
                                    <li><a href="../Agregar/Adestinatario.jsp">Agregar</a></li>
                                    <li><a href="../Modificar/Mdestinatario.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Proveedor</a>
                                <ul>
                                    <li><a href="../Agregar/Aproveedor.jsp">Agregar</a></li>
                                    <li><a href="../Modificar/Mproveedor.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Transportista</a>
                                <ul>
                                    <li><a href="../Agregar/Atransportista.jsp">Agregar</a></li>
                                    <li><a href="../Modificar/Mtransportista.jsp">Modificar</a></li>
                                </ul>
                            </li>
                            <li><a href="../login.jsp?logout">Salir</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content">
                <div id="info">
                    <div id="infobox">
                        <h2>Informaci&oacute;n</h2>
                        <p id="rut_info"></p>
                        <p id="rsocial_info"></p>
                        <p id="direccion_info"></p>
                        <p id="ciudad_info"></p>
                        <p id="telefono_info"></p>
                        <p id="email_info"></p>
                        <p id="div_respuesta">Digite los datos del Proveedor.</p>
                    </div>
                    <h1>Agrega Proveedor</h1>
                    <h2>Ingrese los datos para agregar un nuevo Proveedor</h2>
                    <div id="respond">
                        <form id="form" name="form" method="post" >
                            <p> 
                                <label>RUT</label> 
                                <input maxlength="10" type="text" name="parametro_rut" id="parametro_rut"  />
                            </p>
                            <p>
                                <label>Raz&oacute;n Social</label> 
                                <input maxlength="20" type="text" name="parametro_rsocial" id="parametro_rsocial" />
                            </p>
                            <p>
                                <label>Direcci&oacute;n</label>  
                                <input maxlength="20" type="text" name="parametro_direccion" id="parametro_direccion" /> 
                            </p>
                            <p>
                                <label> C&iacute;udad</label> 
                                <input maxlength="20" type="text" name="parametro_ciudad" id="parametro_ciudad" /> 
                            </p>
                            <p>
                                <label> Tel&eacute;fono</label> 
                                <input maxlength="20" type="text" name="parametro_telefono" id="parametro_telefono" /> 
                            </p>
                            <p>
                                <label> Email</label> 
                                <input maxlength="40" type="text" name="parametro_email" id="parametro_email" />
                            </p>
                            <p>
                                <button class="submit" id="btnReset" type="reset" 
                                    onclick="document.getElementById('div_respuesta').innerHTML = 'Digite los datos del Proveedor.'
                                    ;document.getElementById('rut_info').innerHTML = ''
                                    ;document.getElementById('rsocial_info').innerHTML = ''
                                    ;document.getElementById('direccion_info').innerHTML = ''
                                    ;document.getElementById('ciudad_info').innerHTML = ''
                                    ;document.getElementById('telefono_info').innerHTML = ''
                                    ;document.getElementById('email_info').innerHTML = '';
                                ">Limpiar Formulario</button>      
                                &nbsp;
                                <button class="submit" id="btnGuardarProveedor" type="button" class="button" onclick="guardarProveedor( validaFormulario() );">Registrar Proveedor</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        <br />
    </body>
</html>