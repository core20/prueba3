function validaFormulario() {
    var parametro_rut = document.getElementById("parametro_rut").value;
    var parametro_rsocial = document.getElementById("parametro_rsocial").value;
    var parametro_direccion = document.getElementById("parametro_direccion").value;
    var parametro_ciudad = document.getElementById("parametro_ciudad").value;
    var parametro_telefono = document.getElementById("parametro_telefono").value;
    var parametro_email = document.getElementById("parametro_email").value;
    var parametro_rut_cli = document.getElementById("parametro_rut_cli").value;
    
    bool_rut = validarRut(parametro_rut);
    bool_rsocial = validarRsocial(parametro_rsocial);
    bool_direccion = validarDireccion(parametro_direccion);
    bool_ciudad = validarCiudad(parametro_ciudad);
    bool_telefono = validarTelefono(parametro_telefono);
    bool_email = validarEmail(parametro_email);
    bool_rut_cli = validarRutCliente(parametro_rut_cli);
    
    if (!bool_rut || !bool_rsocial ||!bool_direccion || !bool_ciudad 
        || !bool_telefono || !bool_email || !bool_rut_cli ){
        return false;
    }else{ 
        return true;
    }
}  

function validarRut(rutCompleto) {
    if(rutCompleto.length<1){
        document.getElementById("rut_info").innerHTML = "<span style='color:red'>El campo del rut esta vac&iacute;o</span>";
        return false;
    }else if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto )) {
        document.getElementById("rut_info").innerHTML = "<span style='color:red'>El rut es Incorrecto</span>";
        return false;
    }
    var tmp     = rutCompleto.split('-');
    var digv    = tmp[1];
    var rut     = tmp[0];
    if ( digv == 'K' ){ 
        digv = 'k' ;
    }
    var digesto = dv(rut);
    if (digesto == digv ){
        document.getElementById("rut_info").innerHTML = "<span style='color:green'>El rut es Correcto</span>";
        return true;
    } else {
        document.getElementById("rut_info").innerHTML = "<span style='color:red'>El rut es Incorrecto</span>";
        return false;
        
    }
}

function dv(T){
    var M=0,S=1;
    for(;T;T=Math.floor(T/10)) {
        S=(S+T%10*(9-M++%6))%11;
    }
    return S?S-1:'k';
}

function validarRsocial(parametro_rsocial){
    if(parametro_rsocial.length<1){
        document.getElementById("rsocial_info").innerHTML = "<span style='color:red'>El campo de la raz&oacute;n Social esta vac&iacute;o</span>";
        return false;
    }else if( !( /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s\t-]/.test(parametro_rsocial)) ){
        document.getElementById("rsocial_info").innerHTML = "<span style='color:red'>La raz&oacute;n Social es Incorrecta</span>";
        return false;
    }else{
       document.getElementById("rsocial_info").innerHTML = "<span style='color:green'>La raz&oacute;n Social es Correcta</span>";
       return true;
    }
}

function validarDireccion(parametro_direccion){
    if(parametro_direccion.length<1){
        document.getElementById("direccion_info").innerHTML = "<span style='color:red'>El campo de la Direcci&oacute;n esta vac&iacute;o</span>";
        return false;
    }else if( !( /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s\t-]/.test(parametro_direccion)) ){
        document.getElementById("direccion_info").innerHTML = "<span style='color:red'>La Direcci&oacute;n es Incorrecta</span>";
        return false;
    }else{
       document.getElementById("direccion_info").innerHTML = "<span style='color:green'>La Direcci&oacute;n es Correcta</span>";
       return true;
    }
}

function validarCiudad(parametro_ciudad){
    if(parametro_ciudad.length<1){
        document.getElementById("ciudad_info").innerHTML = "<span style='color:red'>El campo de la C&iacute;udad esta vac&iacute;o</span>";
        return false;
    }else if( !( /[A-Za-zñÑ-áéíóúÁÉÍÓÚ\s\t-]/.test(parametro_ciudad)) ){
        document.getElementById("ciudad_info").innerHTML = "<span style='color:red'>La C&iacute;udad es Incorrecta</span>";
        return false;
    }else{
       document.getElementById("ciudad_info").innerHTML = "<span style='color:green'>La C&iacute;udad es Correcta</span>";
       return true;
    }
}

function validarTelefono(parametro_telefono){
    if(parametro_telefono.length<1){
        document.getElementById("telefono_info").innerHTML = "<span style='color:red'>El campo del Tel&eacute;fono esta vac&iacute;o</span>";
        return false;
    }else if( isNaN(parametro_telefono) || !(/^\d{8}$/.test(parametro_telefono)) ){
        document.getElementById("telefono_info").innerHTML = "<span style='color:red'>El Tel&eacute;fono es Incorrecto</span>";
        return false;
    }else{
        document.getElementById("telefono_info").innerHTML = "<span style='color:green'>El Tel&eacute;fono es Correcto</span>";
        return true;
    }
}

function validarEmail(parametro_email){
    if(parametro_email.length<1){
        document.getElementById("email_info").innerHTML = "<span style='color:red'>El campo del email esta vac&iacute;o</span>";
        return false;
    }else if( !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(parametro_email)) ){
        document.getElementById("email_info").innerHTML = "<span style='color:red'>El email es Incorrecto</span>";
        return false;
    }else{
        document.getElementById("email_info").innerHTML = "<span style='color:green'>El email es Correcto</span>";
        return true;
    }
}

function validarRutCliente(rutCompleto) {
    if(rutCompleto.length<1){
        document.getElementById("rut_info").innerHTML = "<span style='color:red'>El campo del rut del cliente esta vac&iacute;o</span>";
        return false;
    }else if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto )) {
        document.getElementById("rut_cli_info").innerHTML = "<span style='color:red'>El rut del cliente es Incorrecto</span>";
        return false;
    }
    var tmp     = rutCompleto.split('-');
    var digv    = tmp[1];
    var rut     = tmp[0];
    if ( digv == 'K' ){ 
        digv = 'k' ;
    }
    var digesto = dv(rut);
    if (digesto == digv ){
        document.getElementById("rut_info").innerHTML = "<span style='color:green'>El rut del cliente es Correcto</span>";
        return true;
    } else {
        document.getElementById("rut_info").innerHTML = "<span style='color:red'>El rut del cliente es Incorrecto</span>";
        return false;
        
    }
}
