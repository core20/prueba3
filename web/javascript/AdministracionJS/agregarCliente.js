var READY_STATE_COMPLETE = 4;
var peticion_http;

function inicializa_objeto_ajax() {
    // retorno los objetos para trabajo con Ajax segun el navegador       
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            // to create XMLHttpRequest object in later versions
            // of Internet Explorer
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                // to create XMLHttpRequest object in older versions
                // of Internet Explorer
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }// catch
    }
}// fin()

function guardarCliente(bool) {
    if(bool){
        peticion_http = inicializa_objeto_ajax();

        var rut = document.getElementById("parametro_rut").value;
        var rsocial = document.getElementById("parametro_rsocial").value;
        var direccion = document.getElementById("parametro_direccion").value;
        var ciudad = document.getElementById("parametro_ciudad").value;
        var telefono = document.getElementById("parametro_telefono").value;
        var email = document.getElementById("parametro_email").value;
        var dscto = document.getElementById("parametro_dscto").value;
        var clasificacion = document.getElementById("parametro_clasif").value;
        /*var rut_prov = document.getElementById("parametro_rut_prov").value;
        var rut_trans = document.getElementById("parametro_rut_trans").value;
        /*
          var datos = {"parametro_boton":"btnGuardarCliente"
                , "parametro_rut"       : rut 
                , "parametro_rsocial"   : rsocial
                , "parametro_direccion" : direccion 
                , "parametro_ciudad"    : ciudad
                , "parametro_telefono"  : telefono
                , "parametro_email"     : email 
                , "parametro_dscto"     : dscto
                , "parametro_clasif"    : clasificacion};
         */

          var datos = "parametro_boton=btnGuardarCliente"
                + "&parametro_rut="       + rut.replace(/[^\dK]*/gi,'') 
                + "&parametro_rsocial="   + rsocial
                + "&parametro_direccion=" + direccion 
                + "&parametro_ciudad="    + ciudad
                + "&parametro_telefono="  + telefono
                + "&parametro_email="     + email 
                + "&parametro_dscto="     + dscto
                + "&parametro_clasif="    + clasificacion
               /* + "&parametro_rut_prov="  +rut_prov.replace(/[^\dK]*]/gi,'')
                + "&parametro_rut_trans=" +rut_trans.replace(/[^\dK]*]/gi,'')*/;

        peticion_http.onreadystatechange = procesaRespuesta;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

        peticion_http.open("POST", "/Prueba3/S_GestionClientes", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');    
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);

        return; 
    }
 
}// guardarPersona()

function procesaRespuesta() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("div_respuesta").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_respuesta").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_respuesta").innerHTML = "proceso erroneo";
        }
    }
    return;
}//fin()