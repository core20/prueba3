var READY_STATE_COMPLETE = 4;
var peticion_http;
var rut_persona;  //guardamos el rut de busqueda en caso de modificar la primary_key
var id_proceso = 0; //esta variable permite cambiar el texto en div_respuesta
                    //siendo 1 esta buscando y siendo 2 esta modificando

function inicializa_objeto_ajax() {
    // retorno los objetos para trabajo con Ajax segun el navegador       
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            // to create XMLHttpRequest object in later versions
            // of Internet Explorer
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                // to create XMLHttpRequest object in older versions
                // of Internet Explorer
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }// catch
    }
}// fin()

function buscarCliente(){ 
    id_proceso = 1; //seteamos la id al proceso de modificar
    peticion_http = inicializa_objeto_ajax();
    var rut = document.getElementById("parametro_rut").value.replace(/[^\dK]*/gi,'');
    rut_persona = rut; //guardamos el rut de busqueda en caso de modificar la primary_key
    var datos = "parametro_boton=btnBuscarCliente&parametro_rut=" + rut;
    console.log("datos: " + datos);
    
    console.log("antes onready");
    peticion_http.onreadystatechange = procesaRespuesta;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba3/S_GestionClientes", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
    
}//buscarCliente()

function modificarCliente(bool){
    if(bool){
        id_proceso = 2; //seteamos la id al proceso de modificar
        console.log("en  modificarCliente()");
        console.log("peticion_http.readyState: " + peticion_http.readyState);

        peticion_http = inicializa_objeto_ajax();

        var rut = document.getElementById("parametro_rut").value;
        var rsocial = document.getElementById("parametro_rsocial").value;
        var direccion = document.getElementById("parametro_direccion").value;
        var ciudad = document.getElementById("parametro_ciudad").value;
        var telefono = document.getElementById("parametro_telefono").value;
        var email = document.getElementById("parametro_email").value;
        var dscto = document.getElementById("parametro_dscto").value;
        var clasificacion = document.getElementById("parametro_clasif").value;
        /*var rut_prov = document.getElementById("parametro_rut_prov").value;
        var rut_trans = document.getElementById("parametro_rut_trans").value;*/
        
        var datos = "parametro_boton=btnModificarCliente"
                + "&parametro_rut="       + rut.replace(/[^\dK]*/gi,'')
                + "&parametro_rsocial="   + rsocial
                + "&parametro_direccion=" + direccion 
                + "&parametro_ciudad="    + ciudad
                + "&parametro_telefono="  + telefono
                + "&parametro_email="     + email 
                + "&parametro_dscto="     + dscto
                + "&parametro_clasif="    + clasificacion
                + "&parametro_rut_busqueda="+rut_persona
                /*+ "&parametro_rut_prov="  +rut_prov
                + "&parametro_rut_trans=" +rut_trans*/;
                //adicionalmente enviamos el rut de busqueda en caso de modificarse a si mismo ( la  primary_key )
        console.log("datos: " + datos);



        console.log("antes onready");
        peticion_http.onreadystatechange = procesaRespuesta;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
        console.log("despues onready");

        peticion_http.open("POST", "/Prueba3/S_GestionClientes", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');    
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);

        return;
    }
}//modificarCliente()

function eliminarCliente(){    
    peticion_http = inicializa_objeto_ajax();
    var rut = document.getElementById("parametro_rut").value.replace(/[^\dK]*/gi,'');
    var datos = "parametro_boton=btnEliminarCliente&parametro_rut=" + rut;
    console.log("datos: " + datos);
    
    console.log("antes onready");
    peticion_http.onreadystatechange = procesaRespuesta;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba3/S_GestionClientes", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
    
}//buscarCliente()


function procesaRespuesta() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("div_respuesta").innerHTML = "Consultando...";
    }
    else {
        var div_respuesta = document.getElementById("div_respuesta");
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                div_respuesta = document.getElementById("div_respuesta");
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    var datosPersona = respuesta.split("|");
                    console.log(datosPersona);
                    document.getElementById("parametro_rut").value = datosPersona[0]?guionRut(datosPersona[0]):"";
                    document.getElementById("parametro_rsocial").value = datosPersona[1]?datosPersona[1]:"";
                    document.getElementById("parametro_direccion").value = datosPersona[2]?datosPersona[2]:"";
                    document.getElementById("parametro_ciudad").value = datosPersona[3]?datosPersona[3]:"";
                    document.getElementById("parametro_telefono").value = datosPersona[4]?datosPersona[4]:"";
                    document.getElementById("parametro_email").value = datosPersona[5]?datosPersona[5]:"";
                    document.getElementById("parametro_dscto").value = datosPersona[6]?datosPersona[6]:"";
                    document.getElementById("parametro_clasif").value = datosPersona[7]?datosPersona[7]:"";
                    /*document.getElementById("parametro_rut_prov").value = datosPersona[8]?guionRut(datosPersona[8]):"";
                    document.getElementById("parametro_rut_trans").value = datosPersona[9]?guionRut(datosPersona[9]):"";*/
                    if(id_proceso===1) //preguntamos por el id para mostrar el respectivo mensaje del proceso
                        div_respuesta.innerHTML = "Datos de "+datosPersona[1];
                    else if(id_proceso===2)
                        div_respuesta.innerHTML = "Datos modificados correctamente";
                }else {
                    div_respuesta.innerHTML = "No encontrado";
                }
            }
        } else {
            div_respuesta.innerHTML = "Cliente eliminado correctamente.";
        }
    }
    return;
}//fin()

function guionRut(numero){
    return numero.substr(0,numero.length-1)+'-'+numero.substr(-1);
}