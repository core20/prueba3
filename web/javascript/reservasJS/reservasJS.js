var READY_STATE_COMPLETE = 4;
var peticion_http;

function inicializa_objeto_ajax() {
    // retorno los objetos para trabajo con Ajax segun el navegador       
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            // to create XMLHttpRequest object in later versions
            // of Internet Explorer
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                // to create XMLHttpRequest object in older versions
                // of Internet Explorer
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }// catch
    }
}// fin()



//************************************************
//                  FILTRA UBICACIONES (CHECKBOX)
//*************************************************

function busqueda_filtrada(boton) {
    peticion_http = inicializa_objeto_ajax();
    var checkboxes = document.getElementById(id).checkbox; //Array que contiene los checkbox
    var radiobuton = document.getElementById(id).radio;
    var datos = "("; // concadenara la consulta que se envia a la bd
    var contador = 0; // cuenta la cantidad de checkbox activos
    var or_and = " or "; //se agrega un ans o un or dependiendo de los checkbox ke se seleccionen
    var check = "chk"; // nombre del id de los checkbox del formulario 
    var cont_and = 0;

   
    for (var x = 0; x < checkboxes.length; x++) {
        if (checkboxes[x].checked) { // verifica si un checkbox esta o no activado
            contador++;
            // agrega un parentesis y un and para concatenar las consultas
            if (x >= 3 && contador > 1 && cont_and == 0 && contador <= 4) {
                or_and = ") and ";
                datos = datos.substring(0, datos.length - 4) + or_and;
                cont_and++;// cuenta si se ah agregado un and a la conslta
            }
            /*si no se ha seleccion ninguna de las 3 primeras opciones entonces 
             * no agregara ningun and a la sentencia
             * */
            if (x >= 3 && contador == 1) {
                contador = 5;
            }
            /*agrega un or a cada seleccion para poder realizar una busqueda en la tabla*/
            or_and = " or ";
            datos = datos + document.getElementById(check + (x + 1)).value + or_and;


        }
    }
    if (contador != 0) {
        datos = "parametro_boton=" + boton + "&parametro_consulta=" + datos.substring(0, datos.length - 4);
        /*
         * agrega un parentesis al final de la sentencia para evitar un error de sintaxis
         */
        if ((contador <= 3 || contador >= 5) && cont_and == 0) {
            datos = datos + ")";
        }

        console.log("datos: " + datos);
        console.log("antes onready");
        peticion_http.onreadystatechange = procesaRespuestaBu;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
        console.log("despues onready");
        peticion_http.open("POST", "/EdicionBodegasOmar/S_GestionBodegas", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);
    } else {
        alert("no seleciono campos para el filtrado");
    }
    return;
}// busqueda_filtrada()




//************************************************
//  Recibe la repsuesta del servlet, y la muestra en una pagina o capa de la misma
//*************************************************
function procesaRespuestaBu() {
    console.log("en  procesaRespuesta()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === READY_STATE_COMPLETE) {
        if (peticion_http.status === 200) {
            var respuesta = peticion_http.responseText;
            if (respuesta !== "error") {
                document.getElementById("resultado").innerHTML = respuesta;
            }
        }
    } else {
        document.getElementById("resultado").innerHTML = "Buscando...";
    }

    return;
}//fin()
