<%-- 
    Document   : index
    Created on : 25-05-2013, 06:14:08 PM
    Author     : jorge
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="Utilidades.OracleConex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prueba 3 - Bodega</title>
        <link href="images/favicon.ico" rel="shortcut icon">
        <link href="style/index.css" type="text/css" rel="stylesheet">
        <script type="text/javascript">
            var hash = location.hash.substr(1);
            window.onload = function(){
                if(hash === ""){
                    location.hash = "opcion-cliente";
                }
                
                if(hash === "datos-erroneos-clie"){
                    alert("Estimado Cliente:\nFavor de verificar los datos ingresados");
                    location.hash = "opcion-cliente";
                }
                
                if(hash === "datos-erroneos-admin"){
                    alert("Datos incorrectos");
                    location.hash = "opcion-administrador";
                }
                
                if(hash === "logout"){
                    alert("Sesión cerrada!");
                    location.hash = "opcion-cliente";
                }
            };
        </script>
    </head>
    <body>
        <div id="header">
            <img src="images/Logo.png" alt="">
            <h1>Gestión Bodega v0.02</h1>
        </div>
        <div id="content">
            <div id="iniciar-sesion">
                <ul>
                    <li id="opcion-cliente">
                        <a href="#opcion-cliente">Cliente</a>
                        <div>
                            <h3>Iniciar como Cliente</h3>
                            <form id="form-login" name="form-cliente" action="login.jsp" method="POST">
                                <p>
                                    <label for="rut-cliente">Rut:</label><br>
                                    <input type="text" value="" name="rut-cliente" id="rut-cliente">
                                </p>
                                <p>
                                    <label for="pass-cliente">Contraseña</label><br>
                                    <input type="password" value="" name="pass-cliente" id="pass-cliente">
                                </p>
                                <p>
                                    <input type="submit" value="Iniciar" name="cliente" id="cliente">
                                </p>
                            </form>
                        </div>
                    </li>
                    <li id="opcion-administrador">
                        <a href="#opcion-administrador">Administración</a>
                        <div>
                            <h3>Iniciar como Administrador</h3>
                            <form id="form-login" name="form-administrador" action="login.jsp" method="POST">
                                <p>
                                    <label for="nombre-de-usuario">Nombre de usuario:</label><br>
                                    <input type="text" value="" name="nombre-usuario" id="nombre-de-usuario">
                                </p>
                                <p>
                                    <label for="pass-administrador">Contraseña</label><br>
                                    <input type="password" value="" name="pass-administrador" id="pass-administrador">
                                </p>
                                <p>
                                    <input type="submit" value="Entrar" name="administrador" id="administrador">
                                </p>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </body>
</html>