<%
/*Validar que session corresponda*/
if(session.getAttribute("session-tipo") != "clie"){
    response.sendRedirect("index.jsp");
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dar entrada - Bodegaje</title>
        <link href="style/main.css" type="text/css" rel="stylesheet">
        <style type="text/css">
            #resultados li { border-bottom: 1px solid #AAA;}
            #resultados li a { text-decoration: none; color: blue;}
            #resultados li a:hover { text-decoration: none; color: purple; outline: none;}
        </style>
        <script src="javascript/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            window.onload = function(){
                $('#id_bodega').html('<option value="">Cargando...</option>');
                /*getJSON de JQuery que carga URL listaBodegas que retorna el SELECT de las
                 * Bodegas en formato JSON*/
                $.getJSON( "listaBodegas", function( bodegas ) {
                     $('#id_bodega').html('<option></option>');
                     /*Para cada contenido de bodega, recorrer y ejecutar*/
                     $.each(bodegas, function() {
                         $('<option value="'+this.id_bodega+'">'+this.nombre_bodega+'</option>').appendTo('#id_bodega');
                     });
                     $('.id_ubicacion').each(function( index, element ) {
                        $(element).removeAttr('disabled');
                     });
                    });
            };
            
            $('.soloNumero').live('keypress keyup blur', function(){
                /*Expresi�n regular que solo reemplaza d�gitos (letras)*/
                this.value = this.value.replace(/[^\d]*/g,'');
            });
            
            var ubicacionesOptions='';
            function ubicacionesLibresBodega(element){
                if(element.value!==''){
                    $("#id_bodega").attr('disabled','disabled');
                    $('.id_ubicacion').each(function( index, element ) {
                        $(element).html('<option value="">Cargando...</option>');
                    });
                    $.getJSON( "ubicacionesLibres?id_bodega="+element.value, function( ubicaciones ) {
                        options = '<option></option>';
                        $.each(ubicaciones, function() {
                            options += '<option value="'+this.id_ubicacion+'">'+this.id_ubicacion+' '+this.pasillo+' ('+this.ancho+'mts*'+this.ancho+'mts*'+this.altura+'mts)</option>';
                        });
                        //$(element).html(options);
                        agregaUbicacionesLibresBodega(options);
                        $("#id_bodega").removeAttr('disabled');
                        ubicacionesOptions = options;
                    });
                }else{
                    $('.id_ubicacion').each(function( index, element ) {
                        $(element).html('<option></option>');
                    });
                }
            }
            
            function agregaUbicacionesLibresBodega(options){
                $('.id_ubicacion').each(function( index, element ) {
                    if(element.value == ''){
                        $(element).html(options);
                        $(element).removeAttr('disabled');
                    }
                });
            }
            
            /*FUNCION PARA LA BUSQUEDA DE ARTICULOS*/
            function obtenerListadoArticulos(element,e){
                if(e.keyCode===13){
                    $(element).attr('disabled','disabled');
                    $("#busqueda-articulos").css('display','block');
                    $("#busqueda-articulos ol").html('<li>Buscando [ '+element.value+' ]</li>');
                    $.getJSON( "listaArticulos?contenga="+element.value, function( articulos ) {
                        if(articulos.length > 0){
                            $("#busqueda-articulos ol").html('');
                            $.each(articulos, function() {
                                fila = '<li><a href="#" onclick="return agregarArticulo(\''+this.codigo_articulo+'\',\''+this.rut_pvd+'\',\''+this.descripcion+'\',\''+this.rsocial+'\');">'+this.descripcion+' - '+this.rsocial+' / $'+this.precio+' ('+this.largo+'mts*'+this.ancho+'mts*'+this.alto+'mts)</a></li>';
                                $(fila).appendTo('#busqueda-articulos ol');
                            });
                        }else{
                            $("#busqueda-articulos ol").html('<li>�Sin resultados!</li>');
                        }
                        $(element).removeAttr('disabled');
                    });
                    return false;
                }
            }
            
            function agregarArticulo(codigo_articulo,rut_pvd,descripcion,rsocial){
                $('#detalle-entrada tbody').append('<tr>\
                                                        <input type="hidden" name="articulo[]" value="'+codigo_articulo+'">\
                                                        <input type="hidden" name="rut_pvd[]" value="'+rut_pvd+'">\
                                                        <td><input type="text" name="detalle" value="'+descripcion+' - '+rsocial+'" disabled="disabled"></td>\
                                                        <td><input type="text" class="cantidad soloNumero" name="articulo-cantidad[]" value=""></td>\
                                                        <td>\
                                                            <select class="id_ubicacion" name="id_ubicacion[]" onblur="comprobarUbicacionUnica(this);" onclick="comprobarUbicacionUnica(this);" disabled="disabled">\
                                                                <option></option>\
                                                            </select>\
                                                        </td>\
                                                        <td><a href="#" onclick="eliminarArticulo(this);"><img src="images/delete.png" style="height: 24px;"></a></td>\
                                                    </tr>');
                $("#div-detalle-entrada").addClass('active');
                $("#busqueda-articulos").css('display','none');
                agregaUbicacionesLibresBodega(ubicacionesOptions);
                return false;
            }
            
            function eliminarArticulo(element){
                $(element).parents('tr').remove();
            }
            
            function comprobarUbicacionUnica(esteElemento){
                $('.id_ubicacion').each(function( index, element ) {
                    if(esteElemento !== element){
                        if(esteElemento.value === element.value){
                            element.value = '';
                        }
                    }
                });
            }
            
            $('#formEntrada #submit').live('click',function(){
                articulos = [];
                /*Para cada elemento selecciona sus atributos para agregar a array*/
                $('#detalle-entrada [name=articulo[]]').each(function( index, element ){
                    articulo = new Object();
                    articulo.codigo_articulo = element.value;
                    articulo.cantidad = $('#detalle-entrada [name=articulo-cantidad[]]')[index].value;
                    articulo.ubicacion = $('#detalle-entrada [name=id_ubicacion[]]')[index].value;
                    articulo.rut_pvd = $('#detalle-entrada [name=rut_pvd[]]')[index].value;
                    
                    if(articulo.cantidad>0 && articulo.ubicacion!==''){
                        articulos.push(articulo);
                    }else{
                        alert('�No has completado todos los datos!\nNo dejes art�culos vac�os.');
                        return false;
                    }
                });
                //articulos.push({rut:'<%= (String) session.getAttribute("rut")%>'});
                /*console.log(JSON.stringify({
                    articulos:articulos,
                    rut_cliente:'<%= (String) session.getAttribute("rut")%>',
                    codigo_entrada:Math.round((new Date()).getTime() / 1000).toString()}));*/
                $.getJSON( "insertarEntrada?json="+JSON.parse(JSON.stringify(JSON.stringify({
                    articulos:articulos,
                    rut_cliente:'<%= (String) session.getAttribute("rut")%>',
                    codigo_entrada:Math.round((new Date()).getTime() / 1000).toString()}))),
                    function( respuesta ) {
                        if(respuesta['cambios'] > 0){
                            alert("Entrada enviada correctamente!");
                            location.reload();
                        }
                });
                return false;
            });
        </script>
    </head>
    <body style="background: transparent;">
        <div id="registration" style="padding: 10px 30px;">
            <h1>Nueva Entrada</h1>
            <form id="formEntrada" name="formEntrada" action="" method="POST" enctype="multipart/form-data">
                <p>
                    <label>Seleccionar Bodega destino</label>
                    <select id="id_bodega" name="id_bodega" onblur="ubicacionesLibresBodega(this);">
                        <option></option>
                    </select>
                </p>
                <br>
                <input type="hidden" name="cantidad-articulos" value="0">
                <div id="div-detalle-entrada">
                    <label>Detalle</label>
                    <table id="detalle-entrada">
                        <thead>
                            <tr>
                                <th><label>Art�culos</label></th>
                                <th><label>Cantidad</label></th>
                                <th><label>Ubicaci�n disponible</label></th>
                                <th><label>Eliminar</label></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div>
                    <label style="display: inline-block;">Buscar/Agregar</label>
                    <div style="position: relative;">
                        <div id="busqueda-articulos" style="position: absolute; top: 100%; left: 120px; padding: 5px; background-color: #FFF; border: 1px solid #AAA; border-radius: 4px; opacity: 0.9; box-shadow: 0 0 4px 0px rgba(0, 0, 0, 0.5); overflow: auto; display: none;">
                            <ol id="resultados" style="list-style-type: none;">
                            </ol>
                        </div>
                        <input type="text" name="search" value="" placeholder="B�squeda + Enter" onkeypress="return obtenerListadoArticulos(this, event);"> <img src="images/search.png" style="width: 15px; height: 15px;">
                    </div>
                    por: <input type="radio" name="buscar-por" value="sart" checked><label style="display: inline-block;">Art�culo</label> <input type="radio" name="buscar-por" value="sprov"><label style="display: inline-block;">Proveedor</label>
                </div>
                <p>
                    <input id="submit" type="submit" value="Enviar Entrada" name="submit-entrada">
                </p>
            </form>
        </div>
    </body>
</html>